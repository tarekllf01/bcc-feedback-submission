<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomePageConfig extends Model
{
    protected $table = "home_page_configs";

}
