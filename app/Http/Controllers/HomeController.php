<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->role == "SITE ADMIN") {
            return redirect('/siteAdmin');
        }
        elseif (Auth::user()->role == "SUPER ADMIN") {
            return redirect('/superAdmin');
        }
        else if ( Auth::user()->role == "ADMIN") {
            
        }

        return view('home');
    }


}
