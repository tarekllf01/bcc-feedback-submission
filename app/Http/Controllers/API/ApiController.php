<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Category;
use Illuminate\Support\Facades\Hash;
use App\Divission;
use App\District;
use App\Thana;
use App\Crop;
use App\Complain;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
class ApiController extends Controller
{
    public function __construct () {
        $this->middleware('auth:api');
        $this->cellPhoneNumberRegex = "/^(?=.{11}$)(01)\d+$/";

        $this->successStatus = 200;
        $this->createdStatus = 201;
        $this->badRequestStatus = 400;
        $this->unauthorisedStatus = 401;
        $this->forbiddenStatus = 403;
        $this->notFoundStatus = 404;
        $this->notAllowdstatus = 405;
        $this->conflictStatus = 409;
        $this->unprocessableEntry = 422;
        $this->insternalServererror = 500;
        $this->serviceUnavailable = 503;




    }

    public function getProfile () {
        $user = Auth::user();

         return response()->json($user, 200);
    }

    public function getSignUp ( Request $request) {
        $request->validate([
            'mobile'=>'bail|required|unique:t_krishi_users|regex:'.$this->cellPhoneNumberRegex,
            'firstName'=>'nullable|string|max:100',
            'lastName'=>'nullable|string|max:100',
            'email'=>'nullable|email|unique:t_krishi_users',
            'gender'=>'nullable',
            'dateOfBirth'=>'nullable|date',
            'picture'=>'nullable|file|mimes:png,jpg,jpeg|max:2000',
            'divissionId'=>'nullable|integer',
            'districtId'=>'nullable|integer',
            'thanaId'=>'nullable|integer',
            'union'=>'nullable',
            'address'=>'nullable|string',
            'password'=>'required'
        ]);

        // $picture = move_uploaded_file($request, destination)

        $user = new User();
        $user->firstName = $request->firstName;
        $user->lastName = $request->lastName;
        $user->address = $request->address;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->gender = $request->gender;
        $user->dateOfBirth = Carbon::parse($request->dateOfBirth)->format('Y-m-d');
        $user->picture = null;
        $user->divissionId = $request->divissionId;
        $user->districtId = $request->districtId;
        $user->thanaId = $request->thanaId;
        $user->union = $request->union;
        $user->password = bcrypt($request->password);
        $user->status = 1;
        $user->api_token = bin2hex(random_bytes(50));
        $user->role = "user";

        if($user->save()) {
            $response = array();
            $response['message'] = null;
            $response['data'] = null;
            $response['data'] = array();
            $response['data']['api_token'] = $user->api_token;
            return response()->json($response, $this->successStatus);
        } else {
            $response["message"] = "Could not register try again!!";
            return response()->json($response, $this->unprocessableEntry);
        }

    }

    public function getLogin (Request $request) {
        $request->validate([
            'mobile'=>'bail|required|exists:t_krishi_users,mobile',
            'password'=>'required|min:8'
            // 'mobile'=>'bail|required|',
            // 'password'=>'required|'
        ]);

        $response = array();
        $response['message'] = null;
        $response['data'] = null;

        $user = User::where('mobile',$request->mobile)
                    ->where('status',1)
                    ->where('role','user')
                    ->first();
        if( isset($user->password)) {
            if(Hash::check($request->password, $user->password)){
                $data['api_token'] = $user->api_token;
                $response['data'] = $data;
                return response()->json($response, $this->successStatus);
            } else {
                $response['message']  = "Incorrect Login information!";
                return response()->json($response, $this->notFoundStatus);
            }
        } else {
             $response['message'] = " Sorry no user registered with this mobile number";
            return response()->json($response, $this->notFoundStatus);
        }
    }

    public function getDivissions ( ) {
        $response = array();
        $response['message'] = null;
        $response['data'] = null;

        $divissions = Divission::all();
        if(count($divissions) > 0 ) {
            $response['data'] = $divissions;
            return response()->json($response, $this->successStatus);
        } else {
            $response['message'] = "No divissions found";
            return response()->json($response, $this->notFoundStatus);
        }
    }

    public function getDistricts ( Request $request) {
        $request->validate([
            'divissionId'=>'required|exists:t_krishi_divissions,id'
        ]);

        $response = array();
        $response['message'] = null;
        $response['data'] = null;

        $districts = District::where('divissionId',$request->divissionId)->get();
        if(count($districts) > 0 ) {
            $response['data'] = $districts;
            return response()->json($response,$this->successStatus);
        } else {
            $response['message'] = "No Districts found !!";
            return response()->json($response, $this->notFoundStatus);
        }
    }
    public function getThanas ( Request $request) {
        $request->validate([
            'districtId'=>'required|exists:t_krishi_thanas,id'
        ]);
        $response = array();
        $response['message'] = null;
        $response['data'] = null;

        $thanas = Thana::where('districtId',$request->districtId)->get();
        if(count($thanas) > 0 ) {
            $response['data'] = $thanas;
            return response()->json($response,$this->successStatus);
        } else {
            $response['message'] = "No thanas found !!";
            return response()->json($response, $this->notFoundStatus);
        }
    }
    public function getCategories ( ) {
        $response = array();
        $response['message'] = null;
        $response['data'] = null;

        $categories = Category::where('parentCategoryId',NULL)->get();
        if (count($categories) > 0) {
             $response['data'] = $categories;
            return response()->json($response,$this->successStatus);
        } else {
            $response['message'] = "No data found";
            return response()->json($response, $this->notFoundStatus);
        }
    }
    public function getSubCategories ( Request $request) {

        $request->validate([
            'categoryId'=>'required|integer|exists:t_krishi_categories,id',
        ]);

        $response = array();
        $response['message'] = null;
        $response['data'] = null;

        $categories = Category::where('parentCategoryId',$request->categoryId)->get();
        if (count($categories) > 0) {
            $response['data'] = $categories;
            return response()->json($response,$this->successStatus);
        } else {
            $response['message'] = "No Sub category found ";
            return response()->json($response, $this->notFoundStatus);
        }
    }


    public function getCrops (Request $request) {
        $request->validate([
            'categoryId'=>'required|integer|exists:t_krishi_categories,id',
        ]);

        $crops = Crop::where('categoryId',$request->categoryId)->get();

        $response = array();
        $response['message'] = null;
        $response['data']  = null;

        if (count($crops) > 0 ) {
            $response['data'] = $crops;
            return response()->json($response,$this->successStatus);
        } else {
            $response['message'] = 'No data exist!!';
            return response()->json($response,$this->notFoundStatus);
        }
    }

    public function getCropDetail (Request $request) {
        $request->validate([
            'id'=>'required|integer|exists:t_krishi_crops',
        ]);

        $response = array();
        $response['message'] = null;
        $response['data'] = null;

        $details = Crop::where('id',$request->id)->first();
        if ($details) {
            $response['data'] = $details;
            return response()->json($response,$this->successStatus);
        } else {
            $response['message'] = "No details found!";
            return response()->json($response,$this->notFoundStatus);
        }


    }

    public function submitComplain (Request $request) {
        $request->validate([
            'userId'=>'required|exists:t_krishi_users,id',
            'problem'=>'nullable|string',
            'text'=>'nullable|string',
            'picture1'=>'nullable|file|max:2048|mimes:jpg,png,jpeg',
            'picture2'=>'nullable|file|max:2048|mimes:jpg,png,jpeg',
            'audio'=>'nullable|file|max:22048|mimes:mp3',
            'video'=>'nullable|file|max:100048|mimes:mp4,3gp'
        ]);

        $response = array();
        $response['message'] = null;
        $response['data'] = null;


        $complain = new Complain();

        if($request->hasFile('picture1')) {
            $complain->picture1 = $request->file('picture1')->store('complain/picture','public');
        } else {
            $complain->picture1 = null;
        }

        if($request->hasFile('picture2')) {
            $complain->picture2 = $request->file('picture2')->store('complain/picture','public');
        } else {
            $complain->picture2 = null;
        }

        if($request->hasFile('audio')) {
            $complain->audio = $request->file('audio')->store('complain/audio','public');
        } else {
            $complain->audio = null;
        }

        if($request->hasFile('video')) {
            $complain->video = $request->file('video')->store('complain/video','public');
        } else {
            $complain->video = null;
        }

        $complain->text = $request->text;
        $complain->userId = $request->userId;
        $complain->problem = $request->problem;
        $complain->takenAction = 0;
        $complain->done = 0;

        if($complain->save()) {
            $response['message'] = "Succesfully submitted complain!!";
            return response()->json($response,$this->successStatus);
        } else {
            $response['message'] = "Could not sbmit complain!!";
            return response()->json($response,$this->unprocessableEntry);
        }

    }




}
