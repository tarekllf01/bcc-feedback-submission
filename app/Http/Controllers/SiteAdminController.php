<?php

namespace App\Http\Controllers;

use App\Models\HomePageConfig;
use App\Models\ServiceCategory;
use App\Models\TeamMember;
use App\Models\WidgetOne;
use App\Models\WidgetThree;
use App\Models\WidgetTwo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class SiteAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isSiteAdmin');


    }


    public function index() {
        $data = array();
        $data['menu'] = 'home_page_config';

        $settings = HomePageConfig::first();
        $data['settings'] = $settings;
        // print_r($data);
        // echo "hi";

        return view('homePage.siteConfig',$data);
    }


    public function updateSiteConfig ( Request $request) {
        $request->validate([
            'id' => 'required|exists:'.with(new HomePageConfig)->getTable().',id',
            'application_name' => 'required|string',
            'application_details' => 'required|string',
            'nav_bar_logo' => 'nullable|image',
            'site_icon' => 'nullable|image',
            'phone' => 'required|string',
            'backgroundOne' => 'nullable|image',
            'backgroundTwo' => 'nullable|image',
            'backgroundThree' => 'nullable|image',
            'backgroundFour' => 'nullable|image',
            'backgroundFive' => 'nullable|image',

            'youtube_link' => 'required|string',
            'twitter_link' => 'required|string',
            'instagram_link' => 'required|string',
            'play_store_link' => 'required|string',
            'app_store_link' => 'required|string',
        ]);

        $existing = HomePageConfig::where('id',$request->id)->first();
        $data = array(
            'application_name'=>$request->application_name,
            'application_details'=>$request->application_details,
            'phone'=>$request->phone,
            'youtube_link'=>$request->youtube_link,
            'twitter_link'=>$request->twitter_link,
            'instagram_link'=>$request->instagram_link,
            'play_store_link'=>$request->play_store_link,
            'app_store_link'=>$request->app_store_link,

        );
        if ($request->hasFile('nav_bar_logo')) {



            $navbar = $request->file('nav_bar_logo');
            // $navBarLogo = 'nav_bar_logo_'.rand(10,100) . $navbar->getClientOriginalName();
            // $navbar->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$navBarLogo);
            $path = $request->file('nav_bar_logo')->store('images','public');
            $data['nav_bar_logo'] = $path;
        }

        if ($request->hasFile('backgroundOne')) {
            // $backgroundFile = $request->file('backgroundOne');
            // $backgroundOne = 'backgroundOne-'.rand(10,100) . $backgroundFile->getClientOriginalName();
            // $backgroundFile->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$backgroundOne);
            // $data['backgroundOne'] = '/Themes/Bootstrap/images/HelloPage/'.$backgroundOne;

            $path = $request->file('backgroundOne')->store('images','public');
            $data['backgroundOne'] = $path;
        }

        if ($request->hasFile('backgroundTwo')) {
            // $backgroundFile = $request->file('backgroundTwo');
            // $backgroundTwo = 'backgroundTwo-'.rand(10,100) . $backgroundFile->getClientOriginalName();
            // $backgroundFile->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$backgroundTwo);
            // $data['backgroundTwo'] = '\/Themes/Bootstrap/images/HelloPage/'.$backgroundTwo;

            $path = $request->file('backgroundTwo')->store('images','public');
            $data['backgroundTwo'] = $path;
        }

        if ($request->hasFile('backgroundThree')) {
            // $backgroundFile = $request->file('backgroundThree');
            // $backgroundThree = 'backgroundThree-'.rand(10,100) . $backgroundFile->getClientOriginalName();
            // $backgroundFile->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$backgroundThree);
            // $data['backgroundThree'] = '/Themes/Bootstrap/images/HelloPage/'.$backgroundThree;

            $path = $request->file('backgroundThree')->store('images','public');
            $data['backgroundThree'] = $path;
        }

        if ($request->hasFile('backgroundFour')) {
            // $backgroundFile = $request->file('backgroundFour');
            // $backgroundFour = 'backgroundFour-'.rand(10,100) . $backgroundFile->getClientOriginalName();
            // $backgroundFile->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$backgroundFour);
            // $data['backgroundFour'] = '/Themes/Bootstrap/images/HelloPage/'.$backgroundFour;

            $path = $request->file('backgroundFour')->store('images','public');
            $data['backgroundFour'] = $path;
        }

        if ($request->hasFile('backgroundFive')) {
            // $backgroundFile = $request->file('backgroundFive');
            // $backgroundFive = 'backgroundFive-'.rand(10,100) . $backgroundFile->getClientOriginalName();
            // $backgroundFile->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$backgroundFive);
            // $data['backgroundFive'] = '/Themes/Bootstrap/images/HelloPage/'.$backgroundFive;
            $path = $request->file('backgroundFive')->store('images','public');
            $data['backgroundFive'] = $path;

        }

        $update = HomePageConfig::where('id',$request->id)
                                ->update($data);
        if ($update) {
            if (isset($data['nav_bar_logo']))
                try {
                    // unlink(public_path($existing->nav_bar_logo));
                } catch (\Throwable $th) {
                    //throw $th;
                }

            if (isset($backgroundOne))
            try {
                // unlink(public_path($existing->backgroundOne));
            } catch (\Throwable $th) {
                //throw $th;
            }

            if (isset($backgroundTwo))
            try {
                // unlink(public_path($existing->backgroundTwo));
            } catch (\Throwable $th) {
                //throw $th;
            }

            if (isset($backgroundThree))
            try {
                // unlink(public_path($existing->backgroundThree));
            } catch (\Throwable $th) {
                //throw $th;
            }

            if (isset($backgroundFour))
            try {
                // unlink(public_path($existing->backgroundFour));
            } catch (\Throwable $th) {
                //throw $th;
            }

            if (isset($backgroundFive))
            try {
                // unlink(public_path($existing->backgroundFive));
            } catch (\Throwable $th) {
                //throw $th;
            }

            return back()->with('success','Saved');
        }

        return back()->with('fail',' Could not Save');

    }


    public function posts () {

    }

    // ---------------------  service categories --------------------------------------------

    public function seriviceCategories () {
        $categories = ServiceCategory::all();

        $data['menu'] = "service_categories";


        // return view('common.service_categories',$data);
    }


    public function addServiceCategories (Request $request) {
        $request->validate([
            'name' => 'required|unique:'.with(new ServiceCategory)->getTable().',name',
            'details' => 'required',
            'status' => 'nullable|integer|in:0,1'
        ]);

        $serviceCategory = new ServiceCategory();
        $serviceCategory->name = $request->name;
        $serviceCategory->details = $request->details;
        if (isset($request->status))
            $serviceCategory->status = $request->status;

        if ($serviceCategory->save())
            return back()->with('success','added new category');

        return back()->with('fail','Could not add try again');

    }


    public function updateServiceCategories (Request $request) {
        $request->validate([
            'id' => 'required|integer|exists:'.with( new ServiceCategory)->getTable().',id',
            'name' => 'required|unique:'.with(new ServiceCategory)->getTable().',name',
            'details' => 'required',
            'status' => 'nullable|integer|in:0,1'
        ]);


        $value['name'] = $request->name;
        $value['details'] = $request->details;
        if (isset($request->status))
            $value['status'] = $request->status;

        $update = ServiceCategory::where('id',$request->id)
                                ->update($value);
        if ($update)
            return back()->with('success',' updated new category');

        return back()->with('fail','Could not update try again');

    }

    public function deleteSeriviceCategories (Request $request) {
        $request->validate([
            'id' => 'required|integer|exists:'.with( new ServiceCategory)->getTable().',id',
        ]);

        $delete = ServiceCategory::where('id',$request->id)
                                ->delete();

        if ($delete)
            return back()->with('success',' deleted category');
        return back()->with('fail','Could not delete try again');
    }



    //  ------------------------  service categories---------------------------------


    // --------------------------- Manage  Team  ------------------------------------------------

    public function team () {
        $team = TeamMember::all();
        $data['menu'] = "team";
        $data['team'] = $team;

        return view('homePage.team',$data);
    }

    public function updateTeam (Request $request) {
        $request->validate([
            'id' => 'required|exists:'.with(new TeamMember)->getTable().',id',
            'name' => 'required|string',
            'designation' => 'required',
            'picture' => 'nullable|image|',
            'title' => 'required',
            'description' => 'required',
        ]);

        $values['name'] = $request->name;
        $values['designation'] = $request->designation;
        $values['title'] = $request->title;
        $values['description'] = $request->description;

        if ($request->hasFile('picture')) {
            // $picture = $request->file('picture');
            // $pictureFile = 'picture'.rand(100,1000) . $picture->getClientOriginalName();
            // $picture->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$pictureFile);
            // $values['picture'] =  '/Themes/Bootstrap/images/HelloPage/' .$pictureFile;

            $path = $request->file('picture')->store('images','public');
            $values['picture'] = $path;
        }
        $exist = TeamMember::where('id',$request->id)->first();
        $update = TeamMember::where('id',$request->id)
                            ->update($values);

        if ($update) {

            if ($request->hasFile('picture')) {
                try {
                    // unlink(public_path($exist->picture));
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }


            return back()->with('success','  updated');

        }

        return back()->with('fail','Could not  update try again');

    }



    // --------------------  end Team -----------------------------------------

    //  -------------------widget One -----------------------------------------

    public function widgetOne () {
        $widget = WidgetOne::first();

        $data['menu'] = 'widget_one';
        $data['widget'] = $widget;
        return view('homePage.widgetOne',$data);

    }

    public function updateWidgetOne (Request $request) {
        $request->validate([
            'id' => 'required|exists:'.with(new WidgetOne)->getTable().',id',
            'mobile_picture' => 'nullable|image',
            'service_comingsoon_in' => 'required|string',
            'service_available_in' => 'required|string',
            'descriptions' => 'nullabel|string',
        ]);

        $exist = WidgetOne::where('id',$request->id)->get();


        if ($request->hasFile('mobile_picture')) {
            $path = $request->file('mobile_picture')->store('images','public');
            $data['mobile_picture'] = $path;


            // $mobilePicture = $request->file('mobile_picture');
            // $mobilePictureFile = 'mobile_picture'.rand(10,100) . $mobilePicture->getClientOriginalName();
            // $mobilePicture->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$mobilePictureFile);
            // $data['mobile_picture'] =  '/Themes/Bootstrap/images/HelloPage/' .$mobilePictureFile;
        }

        $data['service_available_in'] = $request->service_available_in;
        $data['service_comingsoon_in'] = $request->service_comingsoon_in;
        $data['descriptions'] = $request->descriptions;

        $update = WidgetOne::where('id',$request->id)
                            ->update($data);

        if ($update) {
            if ($request->hasFile('mobile_picture')) {
                try {
                    // unlink(public_path($exist->mobile_picture));
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }

            return back()->with('success','  updated');

        }
        return back()->with('fail','Could not update');
    }



    // ------------------------- widget 2----------------------------------------
    public function widgetTwo () {
        $widget = WidgetTwo::first();

        $data['menu'] = 'widget_one';
        $data['widget'] = $widget;

        return view('homePage.widgetTwo',$data);
    }


    public function updateWidgetTwo (Request $request) {
        $request->validate([
            'id' => 'required|integer|exists:'.with( new WidgetTwo)->getTable().',id',
            'title' => 'required',
            'description' => 'required',
            'mobile_picture' => 'nullable|image',
            'service_one' => 'required',
            'service_one_details' => 'required',
            'service_one_icon' => 'nullable|image',

            'service_two' => 'required',
            'service_two_details' => 'required',
            'service_two_icon' => 'nullable|image',

            'service_three' => 'required',
            'service_three_details' => 'required',
            'service_three_icon' => 'nullable|image',

            'service_four' => 'required',
            'service_four_details' => 'required',
            'service_four_icon' => 'nullable|image',
        ]);
        $widget = WidgetTwo::where('id',$request->id)->first();

        $values = array();

        $values['title'] = $request->title;
        $values['description'] = $request->description;
        $values['service_one'] = $request->service_one;
        $values['service_one_details'] = $request->service_one_details;
        $values['service_two'] = $request->service_two;
        $values['service_two_details'] = $request->service_two_details;
        $values['service_three'] = $request->service_three;
        $values['service_three_details'] = $request->service_three_details;
        $values['service_four'] = $request->service_four;
        $values['service_four_details'] = $request->service_four_details;



        if ($request->hasFile('mobile_picture')) {

            $path = $request->file('mobile_picture')->store('images','public');
            $values['mobile_picture'] = $path;

            // $mobilePicture = $request->file('mobile_picture');
            // $mobilePictureFIleName = 'mobilePictureFIleName-'.rand(10,100) . $mobilePicture->getClientOriginalName();
            // $mobilePicture->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$mobilePictureFIleName);
            // $values['mobile_picture'] =  '/Themes/Bootstrap/images/HelloPage/' .$mobilePictureFIleName;
        }

        if ($request->hasFile('service_one_icon')) {

            $path = $request->file('service_one_icon')->store('images','public');
            $values['service_one_icon'] = $path;


            // $serviceOneIcon = $request->file('service_one_icon');
            // $serviceOneIconName = 'service_one_icon-'.rand(10,100) . $serviceOneIcon->getClientOriginalName();
            // $serviceOneIcon->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$serviceOneIconName);
            // $values['service_one_icon'] =  '/Themes/Bootstrap/images/HelloPage/' .$serviceOneIconName;
        }

        if ($request->hasFile('service_two_icon')) {

            $path = $request->file('service_two_icon')->store('images','public');
            $values['service_two_icon'] = $path;


            // $serviceTwoIcon = $request->file('service_two_icon');
            // $serviceTwoIconName = 'service_two_icon-'.rand(10,100) . $serviceTwoIcon->getClientOriginalName();
            // $serviceTwoIcon->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$serviceTwoIconName);
            // $values['service_two_icon'] =  '/Themes/Bootstrap/images/HelloPage/' .$serviceTwoIconName;
        }

        if ($request->hasFile('service_three_icon')) {

            $path = $request->file('service_three_icon')->store('images','public');
            $values['service_three_icon'] = $path;

            // $serviceThreeIcon = $request->file('service_three_icon');
            // $serviceThreeIconName = 'service_three_icon-'.rand(10,100) . $serviceThreeIcon->getClientOriginalName();
            // $serviceThreeIcon->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$serviceThreeIconName);
            // $values['service_three_icon'] = '/Themes/Bootstrap/images/HelloPage/'.$serviceThreeIconName;
        }

        if ($request->hasFile('service_four_icon')) {
            $path = $request->file('service_four_icon')->store('images','public');
            $values['service_four_icon'] = $path;

            // $serviceFourIcon = $request->file('service_four_icon');
            // $serviceFourIconName = 'service_four_icon-'.rand(10,100) . $serviceFourIcon->getClientOriginalName();
            // $serviceFourIcon->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$serviceFourIconName);
            // $values['service_four_icon'] = '/Themes/Bootstrap/images/HelloPage/'.$serviceFourIconName;
        }


        $update = WidgetTwo::where('id',$request->id)
                            ->update($values);

        if ($update) {

            if ($request->hasFile('mobile_picture')) {
                try {
                    // unlink(public_path($widget->mobile_picture));
                } catch (\Throwable $th) {
                    throw $th;
                }
            }

            if ($request->hasFile('service_one_icon')) {
                try {
                    // unlink(public_path($widget->service_one_icon));
                } catch (\Throwable $th) {
                    throw $th;
                }
            }

            if ($request->hasFile('service_two_icon')) {
                try {
                    // unlink(public_path($widget->service_two_icon));
                } catch (\Throwable $th) {
                    throw $th;
                }
            }

            if ($request->hasFile('service_three_icon')) {
                try {
                    // unlink(public_path($widget->service_three_icon));
                } catch (\Throwable $th) {
                    throw $th;
                }
            }

            if ($request->hasFile('service_four_icon')) {
                try {
                    // unlink(public_path($widget->service_four_icon));
                } catch (\Throwable $th) {
                    throw $th;
                }
            }


            return back()->with('success','  updated!!');
        }

        return back()->with('fail','Sorry Could not updated');

    }

    public function widgetThree () {
        $data['menue'] = "widget_three";
        $data['widget'] = WidgetThree::first();

        return view('homePage.widgetThree',$data);

    }

    public function updateWidgetThree (Request $request) {
        $request->validate([
            'id' => 'required|exists:'.with(new WidgetThree)->getTable().',id',
            'title' => 'required',
            'slider_one' => 'nullable|image',
            'slider_two' => 'nullable|image',
            'slider_three' => 'nullable|image',
            'slider_four' => 'nullable|image',
        ]);

        $values = array();
        $values['title'] = $request->title;



        if ($request->hasFile('slider_one')) {
            $path = $request->file('slider_one')->store('images','public');
            $values['slider_one'] = $path;

            // $sliderOne = $request->file('slider_one');
            // $sliderOneName = 'slider_one-'.rand(10,100) . $sliderOne->getClientOriginalName();
            // $sliderOne->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$sliderOneName);
            // $values['slider_one'] =  '/Themes/Bootstrap/images/HelloPage/' .$sliderOneName;
        }

        if ($request->hasFile('slider_two')) {

            $path = $request->file('slider_two')->store('images','public');
            $values['slider_two'] = $path;

            // $sliderTwo = $request->file('slider_two');
            // $sliderTwoName = 'slider_two-'.rand(10,100) . $sliderTwo->getClientOriginalName();
            // $sliderTwo->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$sliderTwoName);
            // $values['slider_two'] =  '/Themes/Bootstrap/images/HelloPage/' .$sliderTwoName;
        }

        if ($request->hasFile('slider_three')) {
            $path = $request->file('slider_three')->store('images','public');
            $values['slider_three'] = $path;

            // $sliderThree = $request->file('slider_three');
            // $sliderThreeName = 'slider_three-'.rand(10,100) . $sliderThree->getClientOriginalName();
            // $sliderThree->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$sliderThreeName);
            // $values['slider_three'] =  '/Themes/Bootstrap/images/HelloPage/' .$sliderThreeName;
        }

        if ($request->hasFile('slider_four')) {
            $path = $request->file('slider_four')->store('images','public');
            $values['slider_four'] = $path;

            // $sliderFour = $request->file('slider_four');
            // $sliderFourName = 'slider_four-'.rand(10,100) . $sliderFour->getClientOriginalName();
            // $sliderFour->move(public_path('\Themes\Bootstrap\images\HelloPage\\'),$sliderFourName);
            // $values['slider_four'] =  '/Themes/Bootstrap/images/HelloPage/' .$sliderFourName;
        }

        $widget = WidgetThree::where('id',$request->id)->first();
        $update = WidgetThree::where('id',$request->id)
                              ->update($values);
        if ($update) {
            if ($request->hasFile('slider_one')) {
                try {
                    // unlink(public_path($widget->slider_one));
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }
            if ($request->hasFile('slider_two')) {
                try {
                    // unlink(public_path($widget->slider_two));
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }

            if ($request->hasFile('slider_three')) {
                try {
                    // unlink(public_path($widget->slider_three));
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }

            if ($request->hasFile('slider_four')) {
                try {
                    // unlink(public_path($widget->slider_four));
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }


            return back()->with('success','  updated');
        }
        return back()->with('fail','Could not  update try again ');

    }
}
