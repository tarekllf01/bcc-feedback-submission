<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use App\User;
use App\Models\Department;
use App\Models\Feedback;
use Auth;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Dompdf\Option;
use PDF;
use Illuminate\Support\Facades\View;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
        // need to make midle ware for admin only enter

        $this->pdf =new Dompdf;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $data = $this->loadMainDashboard();
        // return $data;
        return view('admin.MainDash',$data);
        $menu = "dashboard";
    }


    public function users(){
        $users = User::all();

        return view('admin.Users',['menu'=>'users','users'=>$users]);
    }

    public function addUser (Request $request) {
        $request->validate([
            'name' => 'required|',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'role' => 'required|'
        ]);

        if (strtoupper($request->role) == 'ADMIN') {
            $request->validate([
                'department_id' => 'required|exists:departments,id'
            ]);
        }
    }

    public function departmentDash (Request $request) {

        $request->validate([
            'id' => 'required|integer|exists:'.with(new Department)->getTable().',id'
        ]);
        $data = array();
        $data = $this->loadMainDashboard($request->id);

        $data['layout'] = 'layouts.application';

        return view('common.Department',$data);

    }

    public function reportPage (Request $request) {
        $data['optionalProjectName'] = "Customer Satisfaction Monitoring System";

        if (isset($request->startDate) && isset($request->endDate)) {
            $startDate = Carbon::parse($request->startDate)->format('Y-m-d');
            $endDate = Carbon::parse($request->endDate)->format('Y-m-d');
        }
        else {
            $startDate = Carbon::now()->firstOfMonth();
            $endDate = Carbon::now()->lastOfMonth();
            $endDate = Carbon::parse($endDate)->format('Y-m-d');
        }

        $data['startDate'] = $startDate;
        $data['endDate'] =$endDate;
        $data['report'] = $this->generateReport($startDate,$endDate);

        // return $data;
        return view('admin.ReportPage',$data);
        // $pdf = PDF::loadView('pdf.ReportPage', $data);
            // return $pdf->stream();

        $policySchedule = App::make('dompdf.wrapper');
        $policySchedule->loadHTML(View::make('admin.ReportPage',$data)->render())->setPaper('a4');
        return $policySchedule->stream();
    }

    public function makeReport (Request $request) {

        if (isset($request->startDate) && isset($request->endDate)) {
            $startDate = Carbon::parse($request->startDate)->format('Y-m-d');
            $endDate = Carbon::parse($request->endDate)->format('Y-m-d');
        }
        else {
            $startDate = Carbon::now()->firstOfMonth();
            $endDate = Carbon::now()->lastOfMonth();
            $endDate = Carbon::parse($endDate)->format('Y-m-d');
        }
        $data['startDate'] = $startDate;
        $data['endDate'] =$endDate;
        $data['report'] = $this->generateReport($startDate,$endDate);

        // $pdf = PDF::loadView('pdf.ReportPage', $data);
        // $pdf->setOption('enable-javascript', true);
        // $pdf->setOption('images', true);
        // $pdf->setOption('javascript-delay', 13000); // page load is quick but even a high number doesn't help
        // $pdf->setOption('enable-smart-shrinking', true);
        // $pdf->setOption('no-stop-slow-scripts', true);
        
        // return $pdf->stream();

        // return view('pdf.ReportPage',$data);
        PDF::setOptions(['isJavascriptEnabled' => true, 'defaultFont' => 'sans-serif']);
        $policySchedule = App::make('dompdf.wrapper');
        $policySchedule->loadHTML(View::make('pdf.ReportPage',$data)->render())->setPaper('a4');

        return $policySchedule->stream();


        $policySchedule = App::make('dompdf.wrapper');
        $policySchedule->loadHTML(View::make('pdf.ReportPage',$data)->render())->setPaper('a4');
        return $policySchedule->stream();

    }

    private function generateReport ($startDate, $endDate) {

        $departments = Department::all();
        $data = array();
        $data['total']['excellent'] = 0;
        $data['total']['veryGood'] = 0;
        $data['total']['average'] = 0;
        $data['total']['notSatisfactory'] = 0;
        $data['total']['total'] = 0;
        foreach ( $departments as $department) {

            //  count for excellent
            $data[$department->id]['name'] = $department->department_name;
            $count = Feedback::where('department_id',$department->id)
                                                            ->whereBetween('created_at',[$startDate,$endDate])
                                                            ->where('react','excellent')
                                                            ->count();

            $data[$department->id]['excellent'] = $count?$count:0;
            $count = 0;

            //  count for very good
            $count = Feedback::where('department_id',$department->id)
                                                            ->whereBetween('created_at',[$startDate,$endDate])
                                                            ->where('react','very good')
                                                            ->count();

            $data[$department->id]['veryGood'] = $count?$count:0;
            $count = 0;

            // count for average
            $count = Feedback::where('department_id',$department->id)
                                                            ->whereBetween('created_at',[$startDate,$endDate])
                                                            ->where('react','average')
                                                            ->count();

            $data[$department->id]['average'] = $count?$count:0;
            $count = 0;

            // count for not satisfactory
            $count = Feedback::where('department_id',$department->id)
                                                            ->whereBetween('created_at',[$startDate,$endDate])
                                                            ->where('react','not satisfactory')
                                                            ->count();

            $data[$department->id]['notSatisfactory'] = $count?$count:0;
            $count = 0;


            $data[$department->id]['total'] = $data[$department->id]['excellent'] + $data[$department->id]['veryGood'] + $data[$department->id]['average'] + $data[$department->id]['notSatisfactory'];
            // $data[$department->id]['']
            $data['total']['excellent'] += $data[$department->id]['excellent'];
            $data['total']['veryGood'] += $data[$department->id]['veryGood'];
            $data['total']['average'] += $data[$department->id]['average'];
            $data['total']['notSatisfactory'] += $data[$department->id]['notSatisfactory'];
            $data['total']['total'] += $data[$department->id]['total'];

        }

        return $data;
    }


    private function loadMainDashboard ($departmentId=null) {
        $department = array();
        if ($departmentId) {
            $department[0] = $departmentId;
        } else {
            $department = Department::select('id')->get();
        }

        $startDate = Carbon::now()->firstOfYear();
        $endDate = Carbon::now()->lastOfYear();
        $endDate = Carbon::parse($endDate)->format('Y-m-d');
        $startOfThisWeak  = Carbon::now()->startOfWeek();
        $endOfThisWeek = Carbon::now()->endOfWeek()->format('y-m-d');



        //  load data for feedback chart

        // return $startOfThisWeak . "to " . $endOfThisWeek;
        $feedBackChartThisWeekData = Feedback::selectRaw('COUNT(*) as numberOfReact, react')
                                ->whereBetween('created_at',[$startOfThisWeak,$endOfThisWeek])
                                ->whereIn('department_id',$department)
                                ->groupBy('react')
                                ->orderByRaw("CASE
                                    WHEN react = 'excellent' THEN 1
                                    WHEN react = 'very good' THEN 2
                                    WHEN react = 'average' THEN 3
                                    WHEN react = 'not satisfactory' THEN 4
                                    ELSE 0
                                    END
                                    ")
                                ->get();
        $totalThisWeek = Feedback::whereBetween('created_at',[$startOfThisWeak,$endOfThisWeek])
                                ->whereIn('department_id',$department)
                                ->count();
        $feedBackChartThisWeek = array();
        $feedBackChartThisWeek['excellent']['number'] = 0;
        $feedBackChartThisWeek['excellent']['perchantage'] = 0;
        $feedBackChartThisWeek['veryGood']['number'] = 0;
        $feedBackChartThisWeek['veryGood']['perchantage'] = 0;
        $feedBackChartThisWeek['average']['number'] = 0;
        $feedBackChartThisWeek['average']['perchantage'] = 0;
        $feedBackChartThisWeek['notSatisfactory']['number'] = 0;
        $feedBackChartThisWeek['notSatisfactory']['perchantage'] = 0;
        if ($feedBackChartThisWeekData)
            foreach ($feedBackChartThisWeekData as $key) {
                switch ($key->react) {
                    case 'excellent':
                        $feedBackChartThisWeek['excellent']['number'] = $key->numberOfReact;
                        $p = $key->numberOfReact * 100 / $totalThisWeek;
                        $feedBackChartThisWeek['excellent']['perchantage'] = round($p,2);
                        break;
                    case 'very good':
                        $feedBackChartThisWeek['veryGood']['number'] = $key->numberOfReact;
                        $p = $key->numberOfReact * 100 / $totalThisWeek;
                        $feedBackChartThisWeek['veryGood']['perchantage'] = round($p,2);
                        break;
                    case 'average':
                        $feedBackChartThisWeek['average']['number'] = $key->numberOfReact;
                        $p = $key->numberOfReact * 100 / $totalThisWeek;
                        $feedBackChartThisWeek['average']['perchantage'] = round($p,2);
                        break;
                    case 'not satisfactory':
                        $feedBackChartThisWeek['notSatisfactory']['number'] = $key->numberOfReact;
                        $p = $key->numberOfReact * 100 / $totalThisWeek;
                        $feedBackChartThisWeek['notSatisfactory']['perchantage'] = round($p,2);
                        break;
                    default:
                        # code...
                        break;
                }
            }

        $data['feedBackChartThisWeek'] = $feedBackChartThisWeek;


        // ---------------------  load data for high  satisfaction of the week --------------------------------
        $startOfThisWeak  = Carbon::parse($startOfThisWeak)->format('Y-m-d');
        $date = $startOfThisWeak;

        $highSatisfactoryThisWeek =  array();
        for ($i =0 ; $i<7;$i++) {
            $dateName = Carbon::parse($date)->isoFormat('dddd');
            $dateValue = Feedback::where('react','excellent')
                                ->where('created_at','like',$date.'%')
                                ->whereIn('department_id',$department)
                                ->count();
            // return $dateValue;
            $highSatisfactoryThisWeek[$dateName] = $dateValue?$dateValue:0;
            $date = Carbon::parse($date)->addDay();
            $date = Carbon::parse($date)->format('Y-m-d');
        }

        $data['highSatisfactoryThisWeek'] = $highSatisfactoryThisWeek;



        //  -------------- load data for last six month ------------------------------------------

        $firstMonth = Carbon::now()->startOfMonth();

        $lastSixMonthReport = array();
        for ($i=0; $i < 6; $i++) {
            $month = Carbon::parse($firstMonth)->format('M Y');
            $compareMonth = Carbon::parse($firstMonth)->format('Y-m');

            // $lastSixMonthReport[$month]['excellent'] = 0;
            // $lastSixMonthReport[$month]['veryGood'] = 0;
            // $lastSixMonthReport[$month]['average'] = 0;
            // $lastSixMonthReport[$month]['notSatisfactory'] = 0;

            $lastSixMonthReport['excellent'][$i]['month'] = $month;
            $lastSixMonthReport['excellent'][$i]['numberOfReact'] = 0;
            $lastSixMonthReport['veryGood'][$i]['month'] = $month;
            $lastSixMonthReport['veryGood'][$i]['numberOfReact'] = 0;
            $lastSixMonthReport['average'][$i]['month'] = $month;
            $lastSixMonthReport['average'][$i]['numberOfReact'] = 0;
            $lastSixMonthReport['notSatisfactory'][$i]['month'] = $month;
            $lastSixMonthReport['notSatisfactory'][$i]['numberOfReact'] = 0;

            $query = Feedback::selectRaw('COUNT(*) as numberOfReact, react')
                            ->where('created_at','like',$compareMonth.'%')
                            ->whereIn('department_id',$department)
                            ->groupBy('react')
                            ->get();
            if ($query)
            foreach ($query as $key) {
                switch ($key->react) {
                    case 'excellent':
                        $lastSixMonthReport['excellent'][$i]['numberOfReact'] = $key->numberOfReact;

                        break;
                    case 'very good':
                        $lastSixMonthReport['veryGood'][$i]['numberOfReact'] = $key->numberOfReact;
                        break;
                    case 'average':
                        $lastSixMonthReport['average'][$i]['numberOfReact'] = $key->numberOfReact;

                        break;
                    case 'not satisfactory':
                        $lastSixMonthReport['notSatisfactory'][$i]['numberOfReact'] = $key->numberOfReact;
                        break;
                    default:
                        # code...
                        break;
                }
            }

            $firstMonth = $firstMonth->subMonth();

        }
        $data['lastSixMonthReport'] = $lastSixMonthReport;

        //  ------------------ department wise report if multiple department -----------------------
        if (count($department) > 1) {
            $departmentWiseData = array();
            $departments = Department::whereIn('id',$department)->get();
            $i=0;

            foreach ($departments as $dep) {

                $departmentWiseData['excellent'][$i]['department'] = strtoupper($dep->department_name);
                $departmentWiseData['excellent'][$i]['perchantage'] = 0;

                $departmentWiseData['veryGood'][$i]['department'] = strtoupper($dep->department_name);
                $departmentWiseData['veryGood'][$i]['perchantage'] = 0;

                $departmentWiseData['average'][$i]['department'] = strtoupper($dep->department_name);
                $departmentWiseData['average'][$i]['perchantage'] = 0;

                $departmentWiseData['notSatisfactory'][$i]['department'] = strtoupper($dep->department_name);
                $departmentWiseData['notSatisfactory'][$i]['perchantage'] = 0;

                // $departmentWiseData[$dep->department_name]['excellent']['number'] = 0;
                // $departmentWiseData[$dep->department_name]['excellent']['perchantage'] = 0;
                // $departmentWiseData[$dep->department_name]['veryGood']['number'] = 0;
                // $departmentWiseData[$dep->department_name]['veryGood']['perchantage'] = 0;
                // $departmentWiseData[$dep->department_name]['average']['number'] = 0;
                // $departmentWiseData[$dep->department_name]['average']['perchantage'] = 0;
                // $departmentWiseData[$dep->department_name]['notSatisfactory']['number'] = 0;
                // $departmentWiseData[$dep->department_name]['notSatisfactory']['perchantage'] = 0;


                $start = Carbon::now()->endOfMonth()->format('Y-m-d');
                $end = Carbon::now()->startOfMonth()->subMonths(6);
                $end = $end->format('Y-m-d');
                $total = Feedback::where('department_id',$dep->id)
                                ->whereBetween('created_at',[$end,$start])
                                ->count();
                $query = Feedback::selectRaw('COUNT(*) as numberOfReact, react')
                                ->where('department_id',$dep->id)
                                ->whereBetween('created_at',[$end,$start])
                                ->groupBy('react')
                                ->get();
                if($query && count($query) > 0 ) {
                    foreach ($query as $key) {
                        switch ($key->react) {
                            case 'excellent':
                               $p = $key->numberOfReact * 100 / $total;
                                $departmentWiseData['excellent'][$i]['perchantage'] = round($p,2);
                                break;
                            case 'very good':
                                $p = $key->numberOfReact * 100 / $total;
                                $departmentWiseData['veryGood'][$i]['perchantage'] = round($p,2);
                                break;
                            case 'average':
                                $p = $key->numberOfReact * 100 / $total;
                                $departmentWiseData['average'][$i]['perchantage'] = round($p,2);
                                break;
                            case 'not satisfactory':
                                $p = $key->numberOfReact * 100 / $total;
                                $departmentWiseData['notSatisfactory'][$i]['perchantage'] = round($p,2);
                                break;
                            default:
                                # code...
                                break;
                        }
                    }
                }
                $i++;

            }

            $data['departmentWiseData'] = $departmentWiseData;

        }  // end if counting department

        return $data;
    }

}
