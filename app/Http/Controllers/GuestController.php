<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Models\HomePageConfig;
use App\Models\TeamMember;
use App\Models\WidgetOne;
use App\Models\WidgetThree;
use App\Models\WidgetTwo;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function index () {

        return view('homePage.en.index');
    }

    public function action (Request $request) {
        $feedback = new Feedback();
        $feedback->react = $request->react;
        $feedback->department_id = $request->department_id;
        $feedback->comment = $request->comment;

        $feedback->save();
        return "true";
    }
}
