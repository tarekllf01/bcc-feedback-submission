<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class IsSiteAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (strtolower(Auth::user()->role) == "site admin" or strtolower(Auth::user()->role) == "super admin"))
            return $next($request);
        return back()->with('fail',' Sorry You do not have permisision');
    }
}
