<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( strtolower(Auth::user()->role) == "admin" or strtolower(Auth::user()->role) == "super admin")
            return $next($request);
        return back()->with('fail',' Sorry You do not have permisision');
    }
}
