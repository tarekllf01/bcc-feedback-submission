
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
           <!--
        *******************************************
        Developer Name: Md. Tarek Hossen
        Developer Email: tarekllf01@gmail.com
        Linked in: https://www.linkedin.com/in/md-tarek-hossen/
        Developer Site: https://tarekhossen.info
        *******************************************
    -->
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">

        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        @if (isset($calendar) && $calendar != false)
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
        @endif
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

        {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> --}}
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}


        <title> Feedback    | {{ Auth::user()->role }} Panel </title>
    </head>
<body class="app sidebar-mini rtl">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('home') }}">
                 @php 
                        if(isset($optionalProjectName)) {
                            echo $optionalProjectName;
                        }
                        else {
                            echo config('app.name', 'Laravel');
                        }
                        
                    @endphp
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item ">
                        <a class="nav-link " title="Information & Communication Technology Division" href="{{route('departmentDash')}}?id=1"> <span class=""> <img src="img/ict.png" width="40px;" height="40px;" alt=""> </span> <span class="row"></span> ICTD  </a>
                    </li>
                    <li class="nav-item ">
                            <a class="nav-link" title="Bangladesh Computer Council " href="{{route('departmentDash')}}?id=2"><span> <img src="img/bcc_logo.png" width="40px;" height="40px;" alt=""> </span><span class="row"></span>  BCC    </a>
                    </li>
                    <li class="nav-item ">
                            <a class="nav-link" title="Bangladesh Hi-Tech Park Authority" href="{{route('departmentDash')}}?id=3"><span> <img src="img/hightec.jpg" width="40px;" height="40px;" alt=""> </span> <span class="row"></span>BHTPA    </a>
                    </li>
                    <li class="nav-item ">
                            <a class="nav-link" title="Department of Information and Communication Technology" href="{{route('departmentDash')}}?id=4"><span> <img src="img/doict.png" width="40px;" height="40px;" alt=""> </span><span class="row"></span> DoICT   </a>
                    </li>
                    <li class="nav-item ">
                            <a class="nav-link" title="Controller of Certifying Authorities" href="{{route('departmentDash')}}?id=5"><span> <img src="img/cca.jpg" width="40px;" height="40px;" alt=""> </span> <span class="row"></span>CCA    </a>
                    </li>
                    <li class="nav-item ">
                            <a class="nav-link" title="Digital Security Agency" href="{{route('departmentDash')}}?id=6"><span> <img src="img/digital.jpg" width="40px;" height="40px;" alt=""> </span><span class="row"></span> DSA    </a>
                    </li>
                    <li class="nav-item ">
                            <a class="nav-link" title="Access to information" href="{{route('departmentDash')}}?id=7"><span> <img src="img/a2i.png" width="40px;" height="40px;" alt=""> </span> <span class="row"></span>A2I    </a>
                    </li>

                    <li class="nav-item ">
                        <a class="nav-link" title="Report " href="{{route('reportPage')}}"> <img src="images/report.png" width="40px;" height="40px;" alt=""> </span> <span class="row"></span>Report       </a>
                    </li>

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>




                    </li>

                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}

                        </a> <span class=""></span>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<br>
    <!-- Sidebar menu-->

    <div class="app-sidebar__overlay" data-toggle="sidebar"> </div>


     @yield('content')



     <!-- Essential javascripts for application to work-->
     <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
     <script src="{{asset('js/popper.min.js')}}"></script>
     <script src="{{asset('js/bootstrap.min.js')}}"></script>
     <script src="{{asset('js/main.js')}}"></script>
     <!-- The javascript plugin to display page loading on top-->
     <script src="{{asset('js/plugins/pace.min.js')}}"></script>
     <!-- Page specific javascripts-->

     <!-- Google analytics script-->
     <script type="text/javascript">
       function resetForm(id){
         document.getElementById("service-request-form").reset();
       }
     </script>
     <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
     <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>

     <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

     <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
 {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> --}}
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
     <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>


     @if (isset($calendar) && $calendar !=false)
     <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
     {!! $events->script() !!}

     <script type="text/javascript">$('#leavePlanner').DataTable();</script>

     @endif


     <script type="text/javascript">
     // $('#sampleTable').DataTable();
     // $('#sampleTable').DataTable( );

 $(document).ready(function() {
     var table = $('#sampleTable').DataTable( {
       // lengthChange: false,
       spans:true,
         buttons: [ 'copy', 'excel', 'colvis' ]
     } );

     table.buttons().container()
         .appendTo( '#sampleTable_wrapper .col-md-6:eq(0)' );
 } );

     </script>
     <script type="text/javascript">
     $(document).ready(function() {
     var table = $('#sampleTable1').DataTable( {
       // lengthChange: false,
       spans:true,
         buttons: [ 'copy', 'excel', 'colvis' ]
     } );

     table.buttons().container()
         .appendTo( '#sampleTable1_wrapper .col-md-6:eq(0)' );
 } );

     </script>

    @yield('piechart')





</body>
</html>

