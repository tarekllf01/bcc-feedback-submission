
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
           <!--
        *******************************************
        Developer Name: Md. Tarek Hossen
        Developer Email: tarekllf01@gmail.com
        Linked in: https://www.linkedin.com/in/md-tarek-hossen/
        Developer Site: https://tarekhossen.info
        *******************************************
    -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css')}}">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        @if (isset($calendar) && $calendar != false)
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
        @endif
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <title> Saw3   | {{ Auth::user()->role }} Panel </title>
    </head>
<body class="app sidebar-mini rtl">
    <header class="app-header" style="background-color:#3D71B8;color:#F7941E;padding:5px;">
        <a class="app-header__logo" href="#" style="background-color:#3D71B8;color:#F7941E;" >
      Saw3  </a>
    <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"> </a>
        <ul class="app-nav">
            <?php $role=Auth::user()->role; ?>
            <li>
          <li  class="app-nav__item"> {{ Auth::user()->firstName . ' ' .Auth::user()->lastName}}</li>
        </li>
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">

            <li><a class="dropdown-item" href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard </a></li>
            <li>
                <a class="dropdown-item" href="{{ route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out fa-lg"></i> {{ __('Logout') }} </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
          </ul>
        </li>
        </ul>
    </header> <br>
    <!-- Sidebar menu-->

    <div class="app-sidebar__overlay" data-toggle="sidebar"> </div>

    <aside class="app-sidebar" style="margin-top:5px;background-color:#3D71B8;color:#F7941E;">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" width="60px;" src="{{ Auth::user()->picture?url('storage/'. Auth::user()->picture):asset('images/avatar.jpg') }}" alt="User Image">
        <div>
          <p class="app-sidebar__user-name" style="color:#E88B2A;"> {{ substr(Auth::user()->firstName,0,16)}}  </p>
          <p class="app-sidebar__user-designation" style="color:#E88B2A;">
            {{ Auth::user()->role }}
           </p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item {{ \Request::route()->getName() == "siteAdmin"?'active':''}} " href="{{route('siteAdmin')}} "><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Home Page</span></a></li>
        {{-- <li><a class="app-menu__item {{ \Request::route()->getName() =="posts"?'active':''}} " href="{{route('posts')}} "><i class="app-menu__icon fa fa-address-card-o"></i><span class="app-menu__label"> Posts </span></a></li> --}}
        {{-- <li><a class="app-menu__item {{ \Request::route()->getName() =="service_categories"?'active':''}}  " href=" {{route('service_categories')}} "><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label"> Service Categories </span></a></li> --}}
        {{-- <li><a class="app-menu__item {{ \Request::route()->getName() =="services"?'active':''}}  " href="{{route('services')}}  "><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label"> Services </span></a></li> --}}
        <li><a class="app-menu__item {{ \Request::route()->getName() =="team"?'active':''}}  " href="{{route('team')}} "><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label"> Team </span></a></li>
        <li><a class="app-menu__item {{ \Request::route()->getName() =="widget_one"?'active':''}}  " href="{{route('widget_one')}} "><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label"> Widget One </span></a></li>
        <li><a class="app-menu__item {{ \Request::route()->getName() =="widget_two"?'active':''}}  " href="{{route('widget_two')}} "><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label"> Widget Two </span></a></li>
        <li><a class="app-menu__item {{ \Request::route()->getName() =="widget_three"?'active':''}}  " href="{{route('widget_three')}} "><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label"> Widget Three </span></a></li>

        <li>
            <a class="app-menu__item" href="{{ route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out fa-lg"></i> {{ __('Logout') }} </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>


      </ul>
    </aside>

     @yield('content')





     <!-- Essential javascripts for application to work-->
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="{{asset('js/plugins/pace.min.js')}}"></script>
    <!-- Page specific javascripts-->

    <!-- Google analytics script-->
    <script type="text/javascript">
      function resetForm(id){
        document.getElementById("service-request-form").reset();
      }
    </script>
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>

    @if (isset($calendar) && $calendar !=false)
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    {!! $events->script() !!}

    <script type="text/javascript">$('#leavePlanner').DataTable();</script>

    @endif


    <script type="text/javascript">$('#sampleTable').DataTable();</script>
    <script type="text/javascript">$('#sampleTable1').DataTable();</script>

    @yield('piechart')





</body>
</html>

