<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    @yield('extra_scripts')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ route('home') }}">
                    
                    @php 
                        if(isset($optionalProjectName)) {
                            echo $optionalProjectName;
                        }
                        else {
                            echo config('app.name', 'Laravel');
                        }
                        
                    @endphp
                    
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item ">
                            <a class="nav-link " title="Information & Communication Technology Division" href="{{route('departmentDash')}}?id=1"> <span class=""> <img src="img/ict.png" width="40px;" height="40px;" alt=""> </span> <span class="row"></span> ICTD  </a>
                        </li>
                        <li class="nav-item ">
                                <a class="nav-link" title="Bangladesh Computer Council " href="{{route('departmentDash')}}?id=2"><span> <img src="img/bcc_logo.png" width="40px;" height="40px;" alt=""> </span><span class="row"></span>  BCC    </a>
                        </li>
                        <li class="nav-item ">
                                <a class="nav-link" title="Bangladesh Hi-Tech Park Authority" href="{{route('departmentDash')}}?id=3"><span> <img src="img/hightec.jpg" width="40px;" height="40px;" alt=""> </span> <span class="row"></span>BHTPA    </a>
                        </li>
                        <li class="nav-item ">
                                <a class="nav-link" title="Department of Information and Communication Technology" href="{{route('departmentDash')}}?id=4"><span> <img src="img/doict.png" width="40px;" height="40px;" alt=""> </span><span class="row"></span> DoICT   </a>
                        </li>
                        <li class="nav-item ">
                                <a class="nav-link" title="Controller of Certifying Authorities" href="{{route('departmentDash')}}?id=5"><span> <img src="img/cca.jpg" width="40px;" height="40px;" alt=""> </span> <span class="row"></span>CCA    </a>
                        </li>
                        <li class="nav-item ">
                                <a class="nav-link" title="Digital Security Agency" href="{{route('departmentDash')}}?id=6"><span> <img src="img/digital.jpg" width="40px;" height="40px;" alt=""> </span><span class="row"></span> DSA    </a>
                        </li>
                        <li class="nav-item ">
                                <a class="nav-link" title="Access to information" href="{{route('departmentDash')}}?id=7"><span> <img src="img/a2i.png" width="40px;" height="40px;" alt=""> </span> <span class="row"></span>A2I    </a>
                        </li>

                        <li class="nav-item ">
                            <a class="nav-link" title="Report " href="{{route('reportPage')}}"> <img src="images/report.png" width="40px;" height="40px;" alt=""> </span> <span class="row"></span>Report       </a>
                        </li>

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>




                        </li>

                        <li class="nav-item">
                            <a class="nav-link " href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}

                            </a> <span class=""></span>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>


    <script>


    </script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
    <script type="text/javascript">$('#sampleTable1').DataTable();</script>
</body>
</html>
