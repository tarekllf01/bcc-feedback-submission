@extends('layouts.universal')

@section('extra_scripts')
    <style type="text/css">
        .canvasjs-chart-credit { display: none; }
        #backButton {
        border-radius: 4px;
        padding: 8px;
        border: none;
        font-size: 16px;
        background-color: #2eacd1;
        color: white;
        position: absolute;
        top: 10px;
        right: 10px;
        cursor: pointer;
        }
        .invisible {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <form action="{{route('reportPage')}} " class="form form-inline" method="post">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="" class="label-control"> Start Date</label>
                        <input type="date" class="form-control" name="startDate" required>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="" class="label-control"> End Date</label>
                        <input type="date" class="form-control" name="endDate" required>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="form-control btn btn-success btn-lg pull-right" > Submit </button>

                </div>

            </div>
            <button type="button" onclick="window.open('{{{route('makeReport').'?startDate=' . $startDate .'&endDate='.$endDate}}}')" class="btn btn-danger btn-lg pull-right " style="margin-left:20px;"> Export To PDF </button>


        </form>

        <hr>
        <div class="alert alert-success">
            The report is showing for {{ \Carbon\Carbon::parse($startDate)->format('d-m-Y') }} -  {{ \Carbon\Carbon::parse($endDate)->format('d-m-Y') }}
        </div>

        <div class="row">
            <table class="table table-hover table-bordered " id="">
                <thead>
                    <th> ID </th>
                    <th> Department  </th>
                    <th> Excellent </th>
                    <th> Very Good </th>
                    <th> Average </th>
                    <th> Not Satisfactory  </th>
                    {{-- <th> Total </th> --}}
                </thead>
                <tbody>
                    @if(count($report))
                        @foreach ($report as $key=>$value)
                            @if (isset($value['name']))
                                <tr>
                                    <td>{{$key}} </td>
                                    <td> {{isset($value['name'])?$value['name']:''}}  </td>

                                    
                                        @php
                                            if ($value['total'] > 0 ) {
                                                echo '<td>' . round($value['excellent']*100/$value['total']) .'% </td>';
                                                echo '<td>' . round($value['veryGood']*100/$value['total']) .'% </td>';
                                                echo '<td>' . round($value['average']*100/$value['total']) .'% </td>';
                                                echo '<td>' . round($value['notSatisfactory']*100/$value['total']) .'% </td>';
                                            } else {
                                                echo '<td> 0 % </td>';
                                                echo '<td> 0 % </td>';
                                                echo '<td> 0 % </td>';
                                                echo '<td> 0 % </td>';
                                            }
                                           
                                        @endphp
                                    {{-- <td> {{$value['total']*100/$value['total']}} </td> --}}
                                </tr>
                            @endif
                            @php
                            @endphp
                        @endforeach

                        <tr style="color:red;font-weight:bold;">
                            <td style="border-right:0px;"> Total </td>
                            <td style="border-left:0px;">  </td>

                            @php
                            if ($value['total'] > 0 ) {
                                echo '<td>' . round($report['total']['excellent']*100/$report['total']['total']) .'% </td>';
                                echo '<td>' . round($report['total']['veryGood']*100/$report['total']['total']) .'% </td>';
                                echo '<td>' . round($report['total']['average']*100/$report['total']['total']) .'% </td>';
                                echo '<td>' . round($report['total']['notSatisfactory']*100/$report['total']['total']) .'% </td>';
                            } else {
                                echo '<td> 0 % </td>';
                                echo '<td> 0 % </td>';
                                echo '<td> 0 % </td>';
                                echo '<td> 0 % </td>';
                            }
                           
                        @endphp


                            {{-- <td> {{round($report['total']['excellent']*100/$report['total']['total'])}} % </td>
                            <td> {{round($report['total']['veryGood']*100/$report['total']['total'])}} % </td>
                            <td> {{round($report['total']['average']*100/$report['total']['total'])}} % </td>
                            <td> {{round($report['total']['notSatisfactory']*100/$report['total']['total'])}} %  </td> --}}
                            {{-- <td> {{ $report['total']['total']}} </td> --}}
                        </tr>
                    @endif


                </tbody>

            </table>
        </div>


    </div>


@endsection
