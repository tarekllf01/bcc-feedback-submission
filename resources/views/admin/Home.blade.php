@extends('layouts.universal')


@section('content')
<main class="app-content">
    <div class="app-title">
        <div>
          <h1><i class="fa fa-cogs"></i> Dashboard </h1>
          <p> Dashboard of Admin   </p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"> </i></li>
        <li class="breadcrumb-item"><a href=""> Dashboard </a></li>
        </ul>
    </div> <!-- end app title -->

    {{-- alert mesage start  --}}
    @if(session('fail'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Error </strong> {{ session('fail')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @if(session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Successful! </strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    {{-- success mesage end  --}}

    {{--  dashboard quick  information --}}


    <div class="tile container-fluid">
        <h3 class="tile-title text-center "> <i class="fa fa-info-circle"></i> Short Information  </h3>
        <div class="tile-body">
            <div class="row">
                <div class="col">
                    <div class="widget-small info "><i class="icon fa fa-users fa-3x"></i>
                        <div class="info">
                            <a href="{{route('users')}}" style="text-decoration:none;color:white">
                                <h4> Users   </h4>
                                <p><b> {{$users?$users:0}} </b></p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="widget-small danger "><i class="icon fa fa-asterisk fa-3x"></i>
                        <div class="info">
                            <a href="{{route('complains')}}" style="text-decoration:none;color:white">
                                <h4> Complains   </h4>
                                <p><b> {{$complains?$complains:0}} </b></p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Complain Line Chart</h3>
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title"> Complains Bar Chart</h3>
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="barChartDemo"></canvas>
            </div>
          </div>
        </div>
      </div>

{{--  quick action start --}}

    @if (Auth::user()->role=="superAdmin")

    <div class="tile container-fluid " >
        <h3 class="tile-title text-center "> <i class="fa fa-gavel "></i> Quick Actions   </h3>
        <div class="tile-body">
            <div class="row quick-actions">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="list-group">
                        <a class="list-group-item" href=" "> <i class="fa fa-plus-square fa-3x"></i> Add Category </a>
                    </div>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="list-group">
                    <a class="list-group-item" href=" "> <i class="fa fa-plus"></i> Add Crop  </a>
                    </div>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="list-group">
                        <a class="list-group-item" href=""> <i class=" fa fa-plus fa-3x "></i> Add news </a>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="list-group">
                        <a class="list-group-item" href=" "> <i class=" fa fa-pencil-square  fa-3x"></i> Manage Complains </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endif
        {{-- quick action end  --}}


    <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">
            <div class="tile" style="height: 560px;">
                <h3 class="tile-title"> Complains Status ratio </h3>

                <div class="embed-responsive embed-responsive-16by9" >
                    <canvas class="embed-responsive-item" id="pieChartDemo"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6" >
            <div class="tile" style="height: 560px;">
                <h3 class="tile-title"> <i class="fa fa-plus "></i> Add News </h3>
                <hr>
                <div class="tile-body "  >
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="serviceType" class="label-control">  Select Crop  </label>
                            <input list="crop" class="form-control" name="crop" >
                            <datalist id="crop">
                                    @if($crops)
                                        @foreach($crops as $crop )
                                            <option value="{{$crop->id}}">{{$crop->name}}</option>
                                        @endforeach
                                    @endif
                                </datalist>
                        </div>
                        <div class="form-group">
                            <label for="title" class="label-control"> Title </label>
                            <input type="text" class="form-control" name="title" required value="{{old('title')}}">
                        </div>
                        <div class="form-group">
                            <label for="subTitle" class="label-control">Sub Title </label>
                            <input type="text" class="form-control" name="subTitle" required value="{{old('subTitle')}}">
                        </div>
                        <div class="form-group">
                            <label for="descriptions" class="label-control"> Descriptions</label>
                            <textarea name="description" id="" cols="30" rows="3" class="form-control"></textarea>
                        </div>

                        <button type="submit" class="btn btn-primary" name="add" >
                            <span><i class="fa fa-plus"></i></span> Add News
                        </button>
                </form>


                    </div>
                </div>
            </div>
        </div>


@endsection

@section('piechart')
<script src="{{asset('js/plugins/chart.js')}}"></script>
<script type="text/javascript">
    var data = {
        labels: ["January", "February", "March", "April", "May"],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 80, 81, 56]
            },
            {
                label: "My Second dataset",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [28, 48, 40, 19, 86]
            }
        ]
    };
    var pdata = [
      	{
      		value: {{ $done?$done:0}},
      		color: "#46BFBD",
      		highlight: "#5AD3D1",
      		label: "Done"
      	},
      	{
      		value: {{ $pending?$pending:0}},
      		color:"#F7464A",
      		highlight: "#FF5A5E",
      		label: "Pendin"
      	}
          ,
      	{
      		value: {{$undone?$undone:0}},
      		color:"#FFCE45",
      		highlight: "#FFC870",
      		label: "Action"
      	}

      ];

    var ctxl = $("#lineChartDemo").get(0).getContext("2d");
    var lineChart = new Chart(ctxl).Line(data);

    var ctxb = $("#barChartDemo").get(0).getContext("2d");
    var barChart = new Chart(ctxb).Bar(data);

    var ctxp = $("#pieChartDemo").get(0).getContext("2d");
    var pieChart = new Chart(ctxp).Pie(pdata);

  </script>
    {{-- <script type="text/javascript">
      var pdata = [
      	{
      		value: 70,
      		color: "#46BFBD",
      		highlight: "#5AD3D1",
      		label: "Done"
      	},
      	{
      		value: 20,
      		color:"#F7464A",
      		highlight: "#FF5A5E",
      		label: "Pendin"
      	}
          ,
      	{
      		value: 20,
      		color:"#FFCE45",
      		highlight: "#FFC870",
      		label: "Action"
      	}

      ];
      var ctxp = $("#pieChartDemo").get(0).getContext("2d");
      var pieChart = new Chart(ctxp).Pie(pdata);
    </script> --}}
@endsection
