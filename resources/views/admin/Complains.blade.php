@extends('layouts.universal')


@section('content')
<main class="app-content">
    <div class="app-title">
        <div>
          <h1><i class="fa fa-cogs"></i> Complains  </h1>
          <p> Manage Complains </p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"> </i></li>
        <li class="breadcrumb-item"><a href=" {{ route('home') }}"> Dashboard </a></li>
        <li class="breadcrumb-item"> Complains  </li>
        </ul>
    </div> <!-- end app title -->


    @if($errors)
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                <strong>  {{$error}} </strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endforeach
    @endif

    {{-- alert mesage start  --}}
    @if(session('fail'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Error </strong> {{ session('fail')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @if(session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Successful! </strong> {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    {{-- success mesage end  --}}



    <div class="tile container-fluid">
        <h3 class="tile-title text-center "> <i class="fa fa-list"></i>  Complains   </h3>
        <div class="tile-body">

            <table class="table table-responsive table-hover table-bordered " id="sampleTable">
                <thead>
                    <th>  ID </th>
                    <th> Name </th>
                    <th> Mobile </th>
                    <th> Text </th>
                    <th> Audio </th>
                    <th> Video  </th>
                    <th> Complain date</th>
                    <th> Status  </th>
                    <th> Action </th>
                </thead>
                <tbody>
                    @if( isset($complains))
                        @foreach ($complains  as $complain )
                        <tr>
                            <td> {{$complain->id}}</td>
                            <td> {{$complain->user->firstName ." " .$complain->user->lastName }}</td>
                            <td>{{ isset($complain->user->mobile)?$complain->user->mobile:'N\A'}} </td>
                            <td>{{ $complain->text?$complain->text:'' }} </td>
                            <td>{{$complain->audio?'<a href="'.asset('storage/complain/'.$complain->audio).'"> link </a>':''}}  </td>
                            <td>{{$complain->video?'<a href="'.asset('storage/complain/'.$complain->video).'"> link </a>':''}}  </td>
                            <td> {{$complain->created_at? Carbon\Carbon::parse($complain->created_at)->format('d-m-Y'):'N/A'}} </td>
                            <td> {{$complain->pending==1?'Pending':'completed'}} </td>
                             <td>
                                <button type="button " class="btn btn-info" data-toggle="modal" data-target="#edit" onclick="editForm({{$complain->id}}" title="Edit">
                                    <i class="fa fa-pencil-square-o"> </i>
                                </button>
                                <button type="button " class="btn btn-danger" data-toggle="modal" data-target="#delete" onclick="attachUrl()" title="Delete">
                                    <i class="fa fa-trash-o"> </i>
                                </button>

                            </td>
                        </tr>
                        @endforeach
                    @endif
                                        <!-- The Modal -->
                    <div class="modal fade" id="delete">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title"> Are You sure want to delete this item? </h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body text-center" >
                                    <a href="" class="btn btn-danger" id="delUrl"> <i class="fa fa-trash-o"> </i> Delete </a>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"> <i class="fa fa-window-close"></i> Cancel </button>
                                </div>
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>






                </tbody>
            </table>
        </div>
    </div>

</main>


<script>
    function editForm(id,value,hod_id){
        document.getElementById('id').value=id;
        document.getElementById('input').value=value;
        document.getElementById('hod_id').value=hod_id;
    }
</script>

<script>
    function attachUrl(url){
        // console.log('testing');
        document.getElementById('delUrl').href= url;
    }
</script>

@endsection
