@extends('layouts.application')

@section('extra_scripts')
    <style type="text/css">
        .canvasjs-chart-credit { display: none; }
        #backButton {
        border-radius: 4px;
        padding: 8px;
        border: none;
        font-size: 16px;
        background-color: #2eacd1;
        color: white;
        position: absolute;
        top: 10px;
        right: 10px;
        cursor: pointer;
        }
        .invisible {
            display: none;
        }
    </style>
    <script>

        window.onload = function () {

            var pieChart = new CanvasJS.Chart("pieChart", {
                animationEnabled: true,
                title:{
                    text: "Feed Backs chart",
                    horizontalAlign: "left"
                },
                data: [{
                    type: "doughnut",
                    startAngle: 60,
                    //innerRadius: 60,
                    indexLabelFontSize: 17,
                    // indexLabel: "{label}(#percent%)",
                    indexLabel: "{label}",
                    toolTipContent: "<b>{label}:</b> ({y}) ",
                    dataPoints: [
                        { y: {{isset($feedBackChartThisWeek['excellent']['number'])?$feedBackChartThisWeek['excellent']['number']:0 }}, label: "Excellent {{isset($feedBackChartThisWeek['excellent']['perchantage'])?$feedBackChartThisWeek['excellent']['perchantage']:0}} % " ,color:"#479242" },
                        { y: {{isset($feedBackChartThisWeek['veryGood']['number'])?$feedBackChartThisWeek['veryGood']['number']:0 }}, label: "Very Good  {{isset($feedBackChartThisWeek['veryGood']['perchantage'])?$feedBackChartThisWeek['veryGood']['perchantage']:0}} % " , color:"#F8DD48"},
                        { y: {{isset($feedBackChartThisWeek['average']['number'])?$feedBackChartThisWeek['average']['number']:0 }}, label: "Average {{isset($feedBackChartThisWeek['average']['perchantage'])?$feedBackChartThisWeek['average']['perchantage']:0}} % ", color:"#FF5009" },
                        { y: {{isset($feedBackChartThisWeek['notSatisfactory']['number'])?$feedBackChartThisWeek['notSatisfactory']['number']:0 }}, label: "Not Satisfactory {{isset($feedBackChartThisWeek['notSatisfactory']['perchantage'])?$feedBackChartThisWeek['notSatisfactory']['perchantage']:0}} % " ,color:"#E02527" },
                    ]
                }]
            });
            pieChart.render();


            //  line chart

            var lineChart = new CanvasJS.Chart("lineChart", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "High Satisfaction This Week"
            },
            axisY:{
                includeZero: false
            },
            data: [{
                type: "line",
                dataPoints: [

                    { y: {{ $highSatisfactoryThisWeek['Monday']}} , label:"Monday"},
                    { y: {{ $highSatisfactoryThisWeek['Tuesday']}} , label:"Tuesday"},
                    { y: {{ $highSatisfactoryThisWeek['Wednesday']}} , label:"Wednesday"},
                    { y: {{ $highSatisfactoryThisWeek['Thursday']}} , label:"Thursday"},
                    { y: {{ $highSatisfactoryThisWeek['Friday']}} , label:"Friday"},
                    { y: {{ $highSatisfactoryThisWeek['Saturday']}} , label:"Saturday"},
                    { y: {{ $highSatisfactoryThisWeek['Sunday']}} , label:"Sunday"},
                    // { y: 520, label:"monday", indexLabel: "highest",markerColor: "#FF5009", markerType: "triangle" },

                    // { y: 410 ,label:"thursday", indexLabel: "lowest",markerColor: "DarkSlateGrey", markerType: "cross" },

                ]
            }]
            });
            lineChart.render();




            //  stack column last six month

            var stackChart = new CanvasJS.Chart("stackChart", {
            animationEnabled: true,
            title:{
                text: "Customer satisfaction summary last 6 month",
                fontFamily: "arial black",
                fontColor: "#695A42"
            },
            axisX: {
                interval: 1,
                intervalType: "month"
            },
            axisY:{
                valueFormatString:"#0",
                gridColor: "#B6B1A8",
                tickColor: "#B6B1A8"
            },
            toolTip: {
                shared: true,
                content: toolTipContent
            },
            data: [

                {
                    type: "stackedColumn",
                    showInLegend: true,
                    name: "Not Satisfactory",
                    color: "#E02527",
                    dataPoints: [
                        { y: {{$lastSixMonthReport['notSatisfactory'][0]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['notSatisfactory'][0]['month'])->format('Y,m-1')}})},
                        { y: {{$lastSixMonthReport['notSatisfactory'][1]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['notSatisfactory'][1]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['notSatisfactory'][2]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['notSatisfactory'][2]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['notSatisfactory'][3]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['notSatisfactory'][3]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['notSatisfactory'][4]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['notSatisfactory'][4]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['notSatisfactory'][5]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['notSatisfactory'][5]['month'])->format('Y,m-1')}}) },
                        // { y: 25, x: new Date(2019,11) },
                        // { y: 0.0, x: new Date(2019,10) },
                        // { y: 0.0, x: new Date(2019,9) },
                        // { y:0.00, x: new Date(2019,8) },
                        // { y: 0.0, x: new Date(2019,7) },
                    ]
                },
                {
                    type: "stackedColumn",
                    showInLegend: true,
                    // color: "#696661",
                    color: "#FF5009",
                    name: "Average",
                    dataPoints: [
                        { y: {{$lastSixMonthReport['average'][0]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['average'][0]['month'])->format('Y,m-1')}})},
                        { y: {{$lastSixMonthReport['average'][1]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['average'][1]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['average'][2]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['average'][2]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['average'][3]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['average'][3]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['average'][4]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['average'][4]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['average'][5]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['average'][5]['month'])->format('Y,m-1')}}) },

                    ]
                },
                {
                    type: "stackedColumn",
                    showInLegend: true,
                    name: "Very Good",
                    color: "#F8DD48",
                    dataPoints: [
                        { y: {{$lastSixMonthReport['veryGood'][0]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['veryGood'][0]['month'])->format('Y,m-1')}})},
                        { y: {{$lastSixMonthReport['veryGood'][1]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['veryGood'][1]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['veryGood'][2]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['veryGood'][2]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['veryGood'][3]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['veryGood'][3]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['veryGood'][4]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['veryGood'][4]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['veryGood'][5]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['veryGood'][5]['month'])->format('Y,m-1')}}) },

                    ]
                },
                {
                    type: "stackedColumn",
                    showInLegend: true,
                    name: "Excellent",
                    // color: "#B6B1A8",
                    color: "#479242",
                    dataPoints: [
                        { y: {{$lastSixMonthReport['excellent'][0]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['excellent'][0]['month'])->format('Y,m-1')}})},
                        { y: {{$lastSixMonthReport['excellent'][1]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['excellent'][1]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['excellent'][2]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['excellent'][2]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['excellent'][3]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['excellent'][3]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['excellent'][4]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['excellent'][4]['month'])->format('Y,m-1')}}) },
                        { y: {{$lastSixMonthReport['excellent'][5]['numberOfReact']}}, x: new Date({{Carbon\Carbon::parse($lastSixMonthReport['excellent'][5]['month'])->format('Y,m-1')}}) },

                    ]
                }]
            });
            stackChart.render();

            function toolTipContent(e) {
                var str = "";
                var total = 0;
                var str2, str3;
                for (var i = 0; i < e.entries.length; i++){
                    var  str1 = "<span style= \"color:"+e.entries[i].dataSeries.color + "\"> "+e.entries[i].dataSeries.name+"</span>: <strong>"+e.entries[i].dataPoint.y+"</strong><br/>";
                    total = e.entries[i].dataPoint.y + total;
                    str = str.concat(str1);
                }
                str2 = "<span style = \"color:DodgerBlue;\"><strong>"+(e.entries[0].dataPoint.x).getFullYear()+"</strong></span><br/>";
                total = Math.round(total * 100) / 100;
                str3 = "<span style = \"color:Tomato\">Total:</span><strong> "+total+"</strong> <br/>";
                return (str2.concat(str)).concat(str3);
            }


            //  line chart with 100 % comparision department wise data


            var allCategories = new CanvasJS.Chart("allCategories", {
                animationEnabled: true,
                title:{
                    text: "Department Wise Satisfaction History"
                },
                axisX: {
                    // interval: 0,
                    // intervalType: "year",
                    // valueFormatString: "YYYY"
                },
                axisY: {
                    suffix: "%"
                },
                toolTip: {
                    shared: true
                },
                legend: {
                    reversed: true,
                    verticalAlign: "center",
                    horizontalAlign: "right"
                },
                data: [{
                    type: "stackedColumn100",
                    name: "Excellent",
                    showInLegend: true,
                    color : "#479242",
                    // xValueFormatString: "YYYY",
                    yValueFormatString: "#,##0\"%\"",
                    dataPoints: [
                        @foreach($departmentWiseData['excellent'] as $depData)
                            { label:"{{$depData['department']}}", y: {{$depData['perchantage']}} },
                        @endforeach

                    ]
                },
                {
                    type: "stackedColumn100",
                    name: "Very Good",
                    color : "#F8DD48",
                    showInLegend: true,
                    // xValueFormatString: "YYYY",
                    yValueFormatString: "#,##0\"%\"",
                    dataPoints: [
                        @foreach($departmentWiseData['veryGood'] as $depData)
                            { label:"{{$depData['department']}}", y: {{$depData['perchantage']}} },
                        @endforeach
                    ]
                },
                {
                    type: "stackedColumn100",
                    name: "Average",
                    showInLegend: true,
                    color : "#FF5009",
                    // xValueFormatString: "YYYY",
                    yValueFormatString: "#,##0\"%\"",
                    dataPoints: [
                        @foreach($departmentWiseData['average'] as $depData)
                            { label:"{{$depData['department']}}", y: {{$depData['perchantage']}} },
                        @endforeach
                    ]
                },
                {
                    type: "stackedColumn100",
                    name: "Not Satisfactory",
                    showInLegend: true,
                    color : "#E02527",
                    // xValueFormatString: "YYYY",
                    yValueFormatString: "#,##0\"%\"",
                    dataPoints: [
                        @foreach($departmentWiseData['notSatisfactory'] as $depData)
                            { label:"{{$depData['department']}}", y: {{$depData['perchantage']}} },
                        @endforeach
                    ]
                },
                ]
            });
            allCategories.render();
        }
    </script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-9">
                <div class="row" style="padding: 5px;">
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                                <div id="pieChart" style="height: 300px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                                <div id="lineChart" style="height: 300px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding: 5px;">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div id="stackChart" style="height: 300px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="row" style="padding: 5px;">
                    <div class="card">
                        <div class="card-body">
                            <p> <strong> Feedback Chart :</strong> Shows the feedbacks in a pie chart of this weeks</p>
                            <p> <strong> High satisfaction this week:</strong> Shows the feedbacks in a line chart comparing other days that week</p>
                            <p> <strong> Customer Satisfaction Summary last 6 Month :</strong> Shows the feedbacks in line chart with others feedbacks marking with different color</p>

                            <p> <strong> Department Wise Satisfaction History :</strong> This section shows the feedbacks of all the divissions and figured at chart with the parcentage of Excellent,Average, Very Good & Not Satisfactory feedbacks.</p>


                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="container-fluid" >
            <div class="card">
                <div class="card-body">
                    <div id="allCategories" style="height: 300px; width: 100%;">

                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

@endsection
