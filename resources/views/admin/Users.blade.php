@extends('layouts.universal')


@section('content')
<main class="app-content">
    <div class="app-title">
        <div>
          <h1><i class="fa fa-cogs"></i> Users  </h1>
          <p> User Details </p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"> </i></li>
        <li class="breadcrumb-item"><a href=" {{ route('home') }}"> Dashboard </a></li>
        <li class="breadcrumb-item"> Users  </li>
        </ul>
    </div> <!-- end app title -->


    @if($errors)
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                <strong>  {{$error}} </strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endforeach
    @endif

    {{-- alert mesage start  --}}
    @if(session('fail'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Error </strong> {{ session('fail')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @if(session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Successful! </strong> {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    {{-- success mesage end  --}}



    <div class="tile container-fluid">
        <h3 class="tile-title text-center "> <i class="fa fa-list"></i>  Users   </h3>
        <div class="tile-body">
            <div class="pull-right">
                <!--<button class="btn" style="background-color:black;color:white;" data-toggle="modal" data-target="#new" title="Create department" > <i class="fa fa-plus"></i> Add Crops </button>-->
            </div>
            <table class="table table-responsive table-hover table-bordered " id="sampleTable">
                <thead>
                    <th> First Name </th>
                    <th> Last Name  </th>
                    <th> mobile </th>
                    <th> gender </th>
                    <th> User's address Information </th>
                    <th> Register Date </th>

                    {{-- @if (Auth::user()->role == "superAdmin") --}}
                    <th> Action </th>
                    {{-- @endif --}}
                </thead>
                <tbody>
                    @if( count($users) > 0)
                        @foreach ($users  as $user )
                        <tr>
                            <td> {{$user->firstName?$user->firstName:''}}</td>
                            <td> {{$user->lastName?$user->lastName:''}}</td>
                            <td> {{$user->mobile?$user->mobile:''}}</td>
                            <td> {{$user->gender?$user->gender:''}}</td>
                            <td> {{$user->address?$user->address:''}}</td>

                            <td> {{$user->created_at?Carbon\Carbon::parse($user->created_at)->format('d-m-Y'):''}}</td>

                            {{-- @if (Auth::user()->role == "superAdmin") --}}
                            <td>
                                <button type="button " class="btn btn-info d-none" data-toggle="modal" data-target="#edit" onclick="editForm({{$user->id}}" title="Edit">
                                    <i class="fa fa-pencil-square-o"> </i>
                                </button>
                                <button type="button " class="btn btn-danger" data-toggle="modal" data-target="#delete" onclick="attachUrl()" title="Delete">
                                    <i class="fa fa-trash-o"> </i>
                                </button>

                            </td>
                            {{-- @endif --}}
                        </tr>
                        @endforeach
                    @endif
                                        <!-- The Modal -->
                    <div class="modal fade" id="delete">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title"> Are You sure want to delete this item? </h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body text-center" >
                                    <a href="" class="btn btn-danger" id="delUrl"> <i class="fa fa-trash-o"> </i> Delete </a>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"> <i class="fa fa-window-close"></i> Cancel </button>
                                </div>
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>






                </tbody>
            </table>
        </div>
    </div>

</main>


<script>
    function editForm(id,value,hod_id){
        document.getElementById('id').value=id;
        document.getElementById('input').value=value;
        document.getElementById('hod_id').value=hod_id;
    }
</script>

<script>
    function attachUrl(url){
        // console.log('testing');
        document.getElementById('delUrl').href= url;
    }
</script>

@endsection
