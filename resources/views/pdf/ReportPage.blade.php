

<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Customer Feedback</title>

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        .page-break {
            page-break-after: always;
        }
    </style>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script>
        window.onload = function () {

            var pieChart = new CanvasJS.Chart("pieChart", {
                animationEnabled: true,
                title:{
                    text: "Feed Backs chart",
                    horizontalAlign: "left"
                },
                data: [{
                    type: "doughnut",
                    startAngle: 60,
                    //innerRadius: 60,
                    indexLabelFontSize: 17,
                    // indexLabel: "{label}(#percent%)",
                    indexLabel: "{label}",
                    toolTipContent: "<b>{label}</b>  ",
                    dataPoints: [
                        { y: {{round($report['total']['excellent']*100/$report['total']['total']) }} , label: "Excellent {{round($report['total']['excellent']*100/$report['total']['total']) }} % " ,color:"#479242" },
                        { y: {{round($report['total']['veryGood']*100/$report['total']['total'])}} , label: "Very Good  {{round($report['total']['veryGood']*100/$report['total']['total'])}} % " , color:"#F8DD48"},
                        { y: {{round($report['total']['average']*100/$report['total']['total'])}} , label: "Average {{round($report['total']['average']*100/$report['total']['total'])}} % ", color:"#FF5009" },
                        { y: {{round($report['total']['notSatisfactory']*100/$report['total']['total'])}} , label: "Not Satisfactory {{round($report['total']['notSatisfactory']*100/$report['total']['total'])}} % " ,color:"#E02527" },
                    ]
                }]
            });
            pieChart.render();
        }


    </script>
</head>
    <body>

        <div class="container-fluid  justify-content-center">
            <div class="row justify-content-center text-center">
                <img src="img/ictd.png" alt="" class="img " width="250px;" style="padding-top:20px;" >

            </div>
            <h3 class="text-center" style="color:green"> Customer Satisfaction Monitoring System</h3>
            <br><br>



            <div class="row">
                <table class="table ">
                    <thead>
                        <tr>
                            <td>Report Generated On : <strong> {{date('d-m-Y')}} </strong> </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td> From : <strong> {{isset($startDate)?Carbon\Carbon::parse($startDate)->format('d-m-Y'):''}} </strong> </td>
                            <td> To : <strong> {{isset($endDate)?Carbon\Carbon::parse($endDate)->format('d-m-Y'):''}}  </strong> </td>
                        </tr>
                    </thead>
                </table>
            </div>

            <div class="row">
                <table class="table  table-bordered" >
                    <thead style="width:100%">
                        <tr>
                            <th> ID </th>
                            <th> Department  </th>
                            <th> Excellent </th>
                            <th> Very Good </th>
                            <th> Average </th>
                            <th> Not Satisfactory  </th>
                            {{-- <th> Total </th> --}}
                        </tr>

                    </thead>
                    <tbody style="">
                        @if(count($report))
                            @foreach ($report as $key=>$value)
                                @if (isset($value['name']))
                                <tr>
                                    <td>{{$key}} </td>
                                    <td> {{isset($value['name'])?$value['name']:''}}  </td>


                                        @php
                                            if ($value['total'] > 0 ) {
                                                echo '<td>' . round($value['excellent']*100/$value['total']) .'% </td>';
                                                echo '<td>' . round($value['veryGood']*100/$value['total']) .'% </td>';
                                                echo '<td>' . round($value['average']*100/$value['total']) .'% </td>';
                                                echo '<td>' . round($value['notSatisfactory']*100/$value['total']) .'% </td>';
                                            } else {
                                                echo '<td> 0 % </td>';
                                                echo '<td> 0 % </td>';
                                                echo '<td> 0 % </td>';
                                                echo '<td> 0 % </td>';
                                            }

                                        @endphp
                                    {{-- <td> {{$value['total']*100/$value['total']}} </td> --}}
                                </tr>
                                @endif
                                @php
                                @endphp
                            @endforeach

                            <tr style="color:red;font-weight:bold;">
                                <td style="border-right:0px;"> Total </td>
                                <td style="border-left:0px;">  </td>

                                @php
                                if ($value['total'] > 0 ) {
                                    echo '<td>' . round($report['total']['excellent']*100/$report['total']['total']) .'% </td>';
                                    echo '<td>' . round($report['total']['veryGood']*100/$report['total']['total']) .'% </td>';
                                    echo '<td>' . round($report['total']['average']*100/$report['total']['total']) .'% </td>';
                                    echo '<td>' . round($report['total']['notSatisfactory']*100/$report['total']['total']) .'% </td>';
                                } else {
                                    echo '<td> 0 % </td>';
                                    echo '<td> 0 % </td>';
                                    echo '<td> 0 % </td>';
                                    echo '<td> 0 % </td>';
                                }

                            @endphp


                                {{-- <td> {{round($report['total']['excellent']*100/$report['total']['total'])}} % </td>
                                <td> {{round($report['total']['veryGood']*100/$report['total']['total'])}} % </td>
                                <td> {{round($report['total']['average']*100/$report['total']['total'])}} % </td>
                                <td> {{round($report['total']['notSatisfactory']*100/$report['total']['total'])}} %  </td> --}}
                                {{-- <td> {{ $report['total']['total']}} </td> --}}
                            </tr>
                        @endif


                    </tbody>

                </table>
            </div>

            <br>
            <br>
            <br>
            <br>
            <strong>
                <h6 style="color:red;">
                    Note: This is system generated report exported from the  Customer Satisfaction Monitoring System.
                </h6>
            </strong>

        </div>

        <div class="page-break">
        </div>

        <div class="container" >
            <div class="card">
                <div class="card-body">
                    <div id="pieChart" style="height: 300px; width: 100%;">

                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>
