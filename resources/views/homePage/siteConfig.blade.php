@extends('layouts.structure')


@section('content')
<main class="app-content">
    <div class="app-title">
        <div>
          <h1><i class="fa fa-cogs"></i> Home Page </h1>
          <p> Configuration   </p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"> </i></li>
        <li class="breadcrumb-item"><a href=""> Configuration </a></li>
        </ul>
    </div> <!-- end app title -->

    {{-- alert mesage start  --}}
    @if(session('fail'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Error </strong> {{ session('fail')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @if(session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Successful! </strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    {{-- success mesage end  --}}

    {{--  dashboard quick  information --}}




    <div class="container-fluid" style="margin-top: 20px;">
        <div class="tile">
            {{-- <h3 class="tile-title"> <i class="fa fa-plus "></i> Add News </h3> --}}
            <div class="tile-body "  >
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{route('updateSiteConfig')}} " method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{$settings->id}}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="label-control">Application Name</label>
                                <input type="text" class="form-control" name="application_name" required value="{{$settings->application_name}}">
                            </div>
                            <div class="form-group">
                                <label for="details" class="label-control">Application Details</label>
                                <input type="text" class="form-control" name="application_details"  value="{{$settings->application_details}}">
                            </div>

                            <div class="form-group">
                                <label for="details" class="label-control"> Phone</label>
                                <input type="text" class="form-control" name="phone"  value="{{$settings->phone}}">
                            </div>

                            <div class="form-group">
                                <label for="details" class="label-control"> Youtube Link</label>
                                <input type="text" class="form-control" name="youtube_link"  value="{{$settings->youtube_link}}">
                            </div>

                            <div class="form-group">
                                <label for="details" class="label-control"> Twitter Link</label>
                                <input type="text" class="form-control" name="twitter_link"  value="{{$settings->twitter_link}}">
                            </div>

                            <div class="form-group">
                                <label for="details" class="label-control"> Instagram  Link</label>
                                <input type="text" class="form-control" name="instagram_link"  value="{{$settings->instagram_link}}">
                            </div>

                            <div class="form-group">
                                <label for="details" class="label-control"> Play store   Link</label>
                                <input type="text" class="form-control" name="play_store_link"  value="{{$settings->play_store_link}}">
                            </div>
                            <div class="form-group">
                                <label for="details" class="label-control"> Appstore  Link</label>
                                <input type="text" class="form-control" name="app_store_link"  value="{{$settings->app_store_link}}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col">
                                    <label for="" class="label-control">Site Logo </label>
                                    <input type="file" class="form-control" name="nav_bar_logo">
                                </div>
                                <div class="col">
                                    <p>Current Logo</p>
                                    <img src="{{asset('storage/'. $settings->nav_bar_logo)}}" alt="" class="img" style="width:150px;">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label for="" class="label-control">Background One </label>
                                    <input type="file" class="form-control" name="backgroundOne">
                                </div>
                                <div class="col">
                                    <p>Current Background</p>
                                    <img src="{{asset('storage/'. $settings->backgroundOne)}}" alt="" class="img" style="width:80px;">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label for="" class="label-control">Background Two </label>
                                    <input type="file" class="form-control" name="backgroundTwo">
                                </div>
                                <div class="col">
                                    <p>Current Background</p>
                                    <img src="{{asset('storage/'. $settings->backgroundTwo)}}" alt="" class="img" style="width:80px;">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label for="" class="label-control">Background Three </label>
                                    <input type="file" class="form-control" name="backgroundThree">
                                </div>
                                <div class="col">
                                    <p>Current Background</p>
                                    <img src="{{asset('storage/'. $settings->backgroundThree)}}" alt="" class="img" style="width:80px;">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label for="" class="label-control">Background Four </label>
                                    <input type="file" class="form-control" name="backgroundFour">
                                </div>
                                <div class="col">
                                    <p>Current Background</p>
                                    <img src="{{asset('storage/'. $settings->backgroundFour)}}" alt="" class="img" style="width:80px;">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label for="" class="label-control">Background Five  </label>
                                    <input type="file" class="form-control" name="backgroundFive">
                                </div>
                                <div class="col">
                                    <p>Current Background</p>
                                    <img src="{{asset('storage/'. $settings->backgroundFive)}}" alt="" class="img" style="width:80px;">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <button type="submit" class="btn btn-primary" name="add" >
                            <span><i class="fa fa-floppy"></i></span> Save Configuration
                        </button>
                    </div>


                </form>
            </div>
        </div>
    </div>


@endsection

@section('piechart')

@endsection
