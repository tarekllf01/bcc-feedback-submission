
<!DOCTYPE html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">


  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> </title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <!-- Custom styles for this template -->
  <link href="css/simple-sidebar.css" rel="stylesheet">
  <link href="css/style1.css" rel="stylesheet">

  <style>


  @font-face {
  font-family: 'Nikosh';
    src: url('/fonts/Nikosh.ttf');
    src: url('/fonts/Nikosh.eot'); /* IE9 Compat Modes */
    src: url('/fonts/Nikosh.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
        url('/fonts/Nikosh.woff2') format('woff2'), /* Modern Browsers */
        url('/fonts/Nikosh.woff') format('woff'), /* Modern Browsers */
        url('/fonts/Nikosh.ttf') format('truetype'), /* Safari, Android, iOS */
        url('/fonts/Nikosh.svg') format('svg'); /* Legacy iOS */
    font-weight: normal;
    font-style: normal;



}

  @import url('https://fonts.maateen.me/charu-chandan-3d/font.css');
      [type=radio] {
      position: absolute;
      opacity: 0;
      width: 0;
      height: 0;



    }

    /* IMAGE STYLES */
    [type=radio] + img {
      cursor: pointer;
    }

    /* CHECKED STYLES */
    [type=radio]:checked + img {
      outline: 2px solid #FF5555;
    }

    input {
      border: 0;
      outline: 0;
      background: transparent;
      border-bottom: 2px solid gray ;
      width: 200px;
    }
    #bangla {
      font-family: 'CharuChandan3D', Arial, sans-serif !important;
    }

    ::-webkit-input-placeholder { /* Edge */
        color: white;
    }

    :-ms-input-placeholder { /* Internet Explorer */
      color: white;
    }

    ::placeholder {
      color: white;
    }
    #myModal {
        background-color: black;
        color:green;
        opacity: 0.6;
    }

    .modal-dialog {
    min-height: calc(100vh - 60px);
    display: flex;
    flex-direction: column;
    justify-content: center;
    overflow: auto;
    }
    @media(max-width: 768px) {
      .modal-dialog {
        min-height: calc(100vh - 20px);
      }
    }

  </style>
  
  <script>
      $(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});
  </script>



</head>

<body>
    <nav class="navbar navbar-expand-sm fixed-top d-flex justify-content-cener text-center" id="topNav">
        <div class="collapse navbar-collapse " id="navbarSupportedContent" >
            <div class="col"></div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-8">
                        <span style="color:white;font-size: 25px;font-weight: bold; text-center;font-family:Nikosh;"> সেবাগ্রহীতাদের মতামত পরিবীক্ষণ ব্যবস্থা </span> <br>
                        <span style="color:white;font-size: 25px;font-weight: bold; text-center;"> (Customer Satisfaction Monitoring System) </span>
                    </div>
                    <div class="col-md-4">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0" style="">
                            <!-- <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li> -->

                            <li class="nav-item" style="padding:10px;" >
                                <img src="{{asset('img/ictNEW.png')}}" alt="" height="60px;">
                            </li>
                            <li class="nav-item" style="padding:10px;">
                                <img src="{{asset('img/top2.png')}}" alt="" height="60px;">
                            </li>
                        </ul>
                    </div>
                </div>


            </div>
            <div class="col"></div>
        </div>
    </nav>

    <div id="loader" class="text-center" style=" width:100%;height:100%;position:absolute;margin-left:10%;margin-top:10%;display: none;opacity: 0.9;">
        <div class="spinner-grow text-primary" style="width: 5rem; height: 5rem;" role="status">
            <span class="sr-only"  >Loading...</span>
          </div>
          <div class="spinner-grow text-secondary" style="width: 5rem; height: 5rem;"  role="status">
            <span class="sr-only">Loading...</span>
          </div>
          <div class="spinner-grow text-success" style="width: 5rem; height: 5rem;"  role="status">
            <span class="sr-only">Loading...</span>
          </div>
          <div class="spinner-grow text-danger" style="width: 5rem; height: 5rem;"  role="status">
            <span class="sr-only">Loading...</span>
          </div>
          <div class="spinner-grow text-warning" style="width: 5rem; height: 5rem;"  role="status">
            <span class="sr-only">Loading...</span>
          </div>
          <div class="spinner-grow text-info" style="width: 5rem; height: 5rem;"  role="status">
            <span class="sr-only">Loading...</span>
          </div>
    </div>
    <div class="container-fluid" style="padding-top:100px;">

    <div class="row" style="">
      <div class="col-2">
          <div class="list-group list-group-flush text-center " style="width:100%;margin-right: 0px;"  id="">

            <a href="#" class="list-group-item list-group-item-action  menuCustom" onclick="switchTab('ict')" style="width:99%;background-color: #;" >
              <div class="row">
                <div class="col "><img id="id1" src="{{asset('img/1red.png')}}" alt="" style="width:115%;height:9vh;"></div>
                <!-- <div class="col d-flex"><h4 class="justify-content-center align-self-center"> ICT Division    </h4></div> -->
              </div>
            </a>

            <a href="#" class="list-group-item list-group-item-action  menuCustom" onclick="switchTab('bcc')" style="width:99%;background-color:;" >
              <div class="row">
                <div class="col"><img id="id2" src="{{asset('img/2.png')}}" alt="" style="width:100%;height:9vh;" ></div>
                <!-- <div class="col d-flex"><h4 class="justify-content-center align-self-center"> Bangladesh Computer Council   </h4></div> -->
              </div>
            </a>
            <a href="#" class="list-group-item list-group-item-action menuCustom" onclick="switchTab('hightec')"  style="width:99%;background-color:;" >
              <div class="row">
                <div class="col"><img id="id3" src="{{asset('img/3.png')}}" alt="" style="width:100%;height:9vh;" ></div>
                <!-- <div class="col d-flex"><h4 class="justify-content-center align-self-center"> Hightech Park  </h4></div> -->
              </div>
            </a>
            <a href="#" class="list-group-item list-group-item-action menuCustom" onclick="switchTab('doict')"  style="width:99%;background-color:;" >
              <div class="row">
                <div class="col"><img id="id4" src="{{asset('img/4.png')}}" alt="" style="width:100%;height:9vh;" ></div>
                <!-- <div class="col d-flex"><h4 class="justify-content-center align-self-center"> Department Of ICT    </h4></div> -->
              </div>
            </a>

            <a href="#" class="list-group-item list-group-item-action menuCustom" onclick="switchTab('cca')"  style="width:99%;background-color:;" >
              <div class="row">
                <div class="col"><img id="id5" src="{{asset('img/5.png')}}" alt="" style="width:100%;height:9vh;" ></div>
                <!-- <div class="col d-flex"><h4 class="justify-content-center align-self-center">  Controller of Certifying Authirity    </h4></div> -->
              </div>
            </a>

            <a href="#" class="list-group-item list-group-item-action menuCustom" onclick="switchTab('dsa')"  style="width:99%;background-color:;" >
              <div class="row">
                <div class="col"><img id="id6" src="{{asset('img/6.png')}}" alt="" style="width:100%;height:9vh;" ></div>
                <!-- <div class="col d-flex"><h4 class="justify-content-center align-self-center">  Digital Securiy Agency     </h4></div> -->
              </div>
            </a>
            <a href="#" class="list-group-item list-group-item-action menuCustom" onclick="switchTab('a2i')"  style="width:99%;background-color:;" >
              <div class="row">
                <div class="col"><img id="id7" src="{{asset('img/7.png')}}" alt="" style="width:100%;height:9vh;" ></div>
                <!-- <div class="col d-flex"><h4 class="justify-content-center align-self-center">  Digital Securiy Agency     </h4></div> -->
              </div>
            </a>
      </div>
      </div>
      <div class="col-10" id="data">
          <div id="data1" style="margin-bottom:0px;">

              <div id="" style="color:white">
                  <h1 class="mt-4" id="titleContent" > Information & Communication Technology Division  </h1>
                  <h3> How satisfied with our service? </h3>
                  <hr>
                  <div class="row text-center justify-content-center" style="margin-top:10vh;margin-bottom:0px;">
                      <form >
                          <input type="hidden" name="department" id="department" required value="1">
                          <label class="text-center">
                              <input type="radio" name="test" value="excellent">
                              <img src="{{asset('img/green.png')}}" alt="" width="350px" id="excellent" onclick="selectButton('excellent')">
                              <h3 class="text-center" style="font-size: 20px;">Excellent </h3>
                          </label>
                          <label class="text-center">
                              <input type="radio" name="test" value="very good">
                              <img src="{{asset('img/yello.png')}}" alt="" width="350px" id="good" onclick="selectButton('good')">
                              <h3 class="text-cen" style="font-size: 20px;"> Very Good </h3>
                          </label>

                          <label class="text-center">
                              <input type="radio" name="test" value="average">
                              <img src="{{asset('img/orange.png')}}" alt="" width="350px" id="average" onclick="selectButton('average')">
                              <h3 class="text-cen" style="font-size: 20px;">Average </h3>
                          </label>

                          <label  class="text-center">
                              <input type="radio" name="test" value="not satisfactory">
                              <img src="{{asset('img/red.png')}}" alt="" width="350px" id="bad" onclick="selectButton('bad')">
                              <h3 class="text-cen" style="font-size: 20px;"> Not Satisfactory </h3>
                          </label>


                          <div class="form-group" style="margin-top:10px; color: white;">
                              <input id ="commentFeild" type="text" style="width:80%;"  name="comment" placeholder="Enter your comment here (Optional)" >
                          </div>
                          <div class="row d-none">
                              <div class="col"> </div>

                               <div class = "col-md-6" id="successMessage" class="text-center justify-content-center">
                                     <?php
                                    if (isset($message) && $message !=="") {
                                        echo '<span class="bg-success" style="padding:10px;" id="" onload="runtime()"> Submitted Successfully </span>';
                                    } else {
                                      echo '<span class="bg-success" id="successMessage"> </span>';
                                      echo "<br> <br> <br><br>";
                                    }
                                  ?>
                              </div>
                              <div class="col"></div>

                            </div>

                          <div class="form-group" >
                              <button id="submitBUtton" style="display:none;"  type="button" name="submit" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal" onclick="showSuccess()"> Submit </button>
                                <!--<button id="submitBUtton" style="display:none;"  type="submit" name="submit" class="btn btn-success btn-lg" onclick="showSuccess()"> Submit </button>-->

                          </div>
                      </form>

                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
<!-- The Modal -->
<div class="modal fade " id="myModal">
    <div class="modal-dialog" style = "background(1,1,1,0.0)">
      <!--<div class="modal-content" style="">-->
        <!-- Modal body -->
        <!--<div class="modal-body" >-->
            <h1 class="text-center" >
                Thank You.
            </h1>
        <!--</div>-->
      <!--</div>-->
    </div>
  </div>

  <!--  modal end -->
  <nav class="navbar navbar-expand-sm fixed-bottom  justify-content-cener" id="bottomNav">
      <span class="text-center" style="color:white;font-weight:bold;font-size:25px;margin-left:47%;"> &copy; 2019 ICT Division </span>
  </nav>
  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('vendor/jquery/jquery.js')}}"></script>
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

  <script>
      function switchTab (tabOpen) {
           $('#commentFeild').val('');
          $('#submitBUtton').css('display','none');
        var tabs = ['ict','bcc','hightec','doict','cca','dsa','a2i'];
        var images = ['ict.png','bcc_logo.png','hightech.png','dict.png','cca.png','digital.png','a2i.png'];
        var menu = ['1.png','2.png','3.png','4.png','5.png','6.png','7.png'];
        var menuActive = ['1red.png','2red.png','3red.png','4red.png','5red.png','6red.png','7red.png'];
        var titleDetails = ['Information and Communication Technology Division','Bangladesh Computer Council ','Bangladesh Hi-Tech Park Authority','Department of Information and Communication Technology','Controller of Certifying Authorities','Digital Security Agency','Access to information '];

        for ( var i=0;i< tabs.length;) {
            $('#'+tabs[i]).css('display','none');
            var urlImage = 'img/' + menu[i];
            i++;

            document.getElementById('id'+i).src =  urlImage;  // change image of nav menu
            $('#id'+i).css('width','100%');
        }
        // $('#loader').css('display','block');
        setTimeout(function(){
            console.log('v');
            var index = tabs.indexOf(tabOpen);
            console.log(index);
            var menuId = index+1;
            console.log(menuId);

            var urlString = 'url(img/' + images[index] + ')';
            var urlActiveImage = 'img/' + menuActive[index];
            console.log(urlActiveImage);
            $('#department').val(menuId);
            document.getElementById('data').style.backgroundImage =  urlString;
            document.getElementById('id'+menuId).src =  urlActiveImage;  // change image of nav menu
            $('#id'+menuId).css('width','115%');  // increase wdth of active nav menu
            // $('#loader').css('display','none');
            // $('#'+tabOpen).css('display','block');
            $('#titleContent').text(titleDetails[index]);
            $('#submitBUtton').attr('disabled','disabled');
            $('#excellent').css('width','350px');
            $('#good').css('width','340px');
            $('#average').css('width','350px');
            $('#bad').css('width','350px');

            console.log('after');
        },200);



      }
  </script>




    <script>
      function showSuccess () {
        const ut = new SpeechSynthesisUtterance('Thank You');
        speechSynthesis.speak(ut);
          
        var comment = $('#commentFeild').val();
        var react = $('input[name=test]:checked').val();
        $('#commentFeild').val('');
        var department = $('#department').val();
        $('#submitBUtton').css('display','none');
        $('#excellent').css('width','350px');
        $('#good').css('width','350px');
        $('#average').css('width','350px');
        $('#bad').css('width','350px');

        console.log(comment);
        console.log(react);
        console.log(department);

        var url = '{{route("action")}}?react='+ react + '&department_id=' + department + '&comment=' + comment;
        console.log(url);
        $.ajax({
            url: url,
            cache: false,
            success: function(data){
                console.log(data);
            }
        });
        // $('#myModal').modal('close');

        setTimeout(function(){$('#myModal').modal('hide')},1500);

      }

    </script>

    <script>
      function selectButton (button) {
        $('#submitBUtton').css('display','');
        $('#excellent').css('width','350px');
        $('#good').css('width','350px');
        $('#average').css('width','350px');
        $('#bad').css('width','350px');

        $('#'+button).css('width','430px');

        $('#submitBUtton').removeAttr('disabled');
      }
    </script>

      <script>

        setTimeout(function() {
          $('#successMessage').css('display','none');

        }, 3000);

  </script>

</body>

</html>
