<?php

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'GuestController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//  home page configurations
Route::get('/siteAdmin', 'SiteAdminController@index')->name('siteAdmin');

Route::get('/superAdmin', 'AdminController@index')->name('superAdmin');

Route::any('/reportPage','AdminController@reportPage')->name('reportPage');

Route::get('/makeReport','AdminController@makeReport')->name('makeReport');

Route::get('/departmentDash','AdminController@departmentDash')->name('departmentDash');

Route::get('/action','GuestController@action')->name('action');
