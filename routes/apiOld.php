<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('getSignUp','API\ApiController@getSignUp')->name('getSignUp');
Route::post('getLogin','API\ApiController@getLogin')->name('getLogin');
Route::get('getProfile','API\ApiController@getProfile')->name('getProfile');

Route::get('getDivissions','API\ApiController@getDivissions')->name('getDivissions');
Route::post('getDistricts','API\ApiController@getDistricts')->name('getDistricts');
Route::post('getThanas','API\ApiController@getThanas')->name('getThanas');
Route::get('getCategories','API\ApiController@getCategories')->name('getCategories');
Route::POST('getSubCategories','API\ApiController@getSubCategories')->name('getSubCategories');
