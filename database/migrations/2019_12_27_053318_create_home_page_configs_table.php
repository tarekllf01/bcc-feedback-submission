<?php

use App\Models\HomePageConfig;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomePageConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(with(new HomePageConfig)->getTable(), function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('application_name');
            $table->string('application_details')->nullable();
            $table->string('nav_bar_logo');
            $table->string('site_icon')->nullable();
            $table->string('phone');
            $table->string('backgroundOne');
            $table->string('backgroundTwo');
            $table->string('backgroundThree');
            $table->string('backgroundFour');
            $table->string('backgroundFive');
            $table->string('youtube_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('play_store_link')->nullable();
            $table->string('app_store_link')->nullable();
            $table->timestamps();
        });

        $homePage = new HomePageConfig();

        $homePage->application_name = "Saw3";
        $homePage->application_details = "Home Service";
        $homePage->nav_bar_logo = asset('Themes/Bootstrap/images/mainlogo.png');
        $homePage->site_icon = null;
        $homePage->phone = "+966XXXXXX";
        $homePage->backgroundOne = asset('Themes/Bootstrap/images/HelloPage/6.jpg');
        $homePage->backgroundTwo = asset('Themes/Bootstrap/images/HelloPage/DSC03533.jpg');
        $homePage->backgroundThree = asset('Themes/Bootstrap/images/HelloPage/DSC03557.jpg');
        $homePage->backgroundFour = asset('Themes/Bootstrap/images/HelloPage/plumb.jpg');
        $homePage->backgroundFive = asset('Themes/Bootstrap/images/HelloPage/DSC03629.jpg');
        $homePage->youtube_link = "#";
        $homePage->twitter_link = "#";
        $homePage->instagram_link = "#";
        $homePage->play_store_link = "#";
        $homePage->app_store_link = "#";

        $homePage->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(with(new HomePageConfig)->getTable());
    }
}
