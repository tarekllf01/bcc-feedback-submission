<?php
use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(with(new User)->getTable(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('role');
            $table->string('api_token')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->tinyInteger('status');
            $table->rememberToken();
            $table->timestamps();
        });

        $user = new User();
        $user->name = "Md. Tarek Hossen";
        $user->email = "superadmin@gmail.com";
        $user->role = "SUPER ADMIN";
        $user->api_token = bin2hex(random_bytes(50));
        $user->password =  Hash::make('password');
        $user->status = 1;
        $user->save();

        $user = new User();
        $user->name = "MR ADMIN";
        $user->email = "admin@gmail.com";
        $user->role = "ADMIN";
        $user->api_token = bin2hex(random_bytes(50));
        $user->password =  Hash::make('password');
        $user->status = 1;
        $user->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(with(new User)->getTable());
    }
}
